import "neonbroom"
import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"

local gfx <const> = playdate.graphics


function getGunBulletTemplate(gunType)
  local details = gunDetails[gunType]
  
  local template = {}
    template.speed = details.baseSpeed + getUpgradeContribution(gunType, "bulletspeed")
    template.rotation = 0
    template.damage = details.baseDamage + getUpgradeContribution(gunType, "damage")
    template.shotCooldown = details.baseShotCooldown + getUpgradeContribution(gunType, "shotcooldown")
    template.size = details.baseSize + getUpgradeContribution(gunType, "shotsize")
    template.lockOnTime = details.baseLockOnTime + getUpgradeContribution(gunType, "lockonspeed")
    template.chewTime = details.baseChewTime + getUpgradeContribution(gunType, "chewtime")
    template.animations = playerAssets.bullets[gunType].animations
    template.shrapnelCount = (details.baseShrapnelCount or 0) + getUpgradeContribution(gunType, "shrapnelcount")
    template.fliesStraight = details.fliesStraight
    template.maxRange = (details.baseMaxRange or 0) + getUpgradeContribution(gunType, "maxrange")
    template.name = gunType
    template.speedsUpIfNecessary = true
    if template.shrapnelCount > 0 then
      template.shrapnelBulletTemplate = getGunBulletTemplate(details.shrapnelBullet)
    end
    
  return template;
end

function makeBullet(bulletTemplate, target, source)
  local bullet = {}
  
  for k, v in pairs(bulletTemplate) do
      bullet[k] = v
  end
  
  bullet.source = source
  
  bullet.guiRadarBlip = {}
    bullet.guiRadarBlip.animations = playerAssets.bulletRadarBlip.animations
    neonbroom.setState(bullet.guiRadarBlip, "on")
    bullet.guiRadarBlip.xPosPlaydate = neonbroom.radarBackground.xPosPlaydate
    bullet.guiRadarBlip.yPosPlaydate = neonbroom.radarBackground.yPosPlaydate
    bullet.guiRadarBlip.widthPlaydate, bullet.guiRadarBlip.heightPlaydate = bullet.guiRadarBlip.animations["on"][1]:getSize()
    bullet.guiRadarBlip.angle = 0

  bullet.theta = source and source.theta or player.currentFacing--target.theta
  bullet.distance = source and source.distance or 0.01
  bullet.height = source and source.height or 100
  bullet.angle = 0
  bullet.tVelocity = 0
  bullet.hVelocity = 0
  bullet.dVelocity = 0
  
  neonbroom.setState(bullet, "flying")

  bullet.usesDirectionalRotation = true
  
  if (target.animations) then bullet.target = player.target end -- tracking!
  
  neonbroom.updateVelocityFromTo(bullet, target)
  
  table.insert(level.bullets, bullet)
  
  if not source then updateStatsInProfile(bulletTemplate.name.."_Shot", 1) end
end

function getUpgradeContribution(gunType, gunUpgrade)
  local upgradeName = gunType:lower()..gunUpgrade
  local upgradeLevel = getUpgradeLevel(upgradeName)
  local upgrade = upgradeDetails[upgradeName]
  
  if not upgrade then return 0 end
  
  local maxLevel = upgrade.maxLevel
  local maxBenefit = upgrade.maxBenefit
  
  return maxBenefit / maxLevel * upgradeLevel
end

function getAimingReticle()
  local aimingReticleTemplate = {}
    aimingReticleTemplate.xPosPlaydate = neonbroom.ratios.playdate.width / 2 * neonbroom.ratios.playdate.playAreaWidthScale
    aimingReticleTemplate.yPosPlaydate = neonbroom.ratios.playdate.height / 2
    aimingReticleTemplate.velocity = neonbroom.ratios.playdate.width * 0.50 + getUpgradeContribution("", "aimingreticlespeed")
    aimingReticleTemplate.lockOnRange = 10
    aimingReticleTemplate.maxTargetsToCycle = 3
    
    aimingReticleTemplate.animations = playerAssets.aimingReticle.animations
    neonbroom.setState(aimingReticleTemplate, "aiming")
    
    aimingReticleTemplate.widthPlaydate, aimingReticleTemplate.heightPlaydate = aimingReticleTemplate.animations["aiming"][1]:getSize()
    aimingReticleTemplate.angle = 0
  
  return aimingReticleTemplate
end

function getLockOnReticle()
  local lockOnReticleTemplate = {}
    lockOnReticleTemplate.tVelocity = 0
    lockOnReticleTemplate.hVelocity = 0
    lockOnReticleTemplate.dVelocity = 0
    
    lockOnReticleTemplate.animations = playerAssets.lockOnReticle.animations
    neonbroom.setState(lockOnReticleTemplate, "locked")
    
    lockOnReticleTemplate.widthPlaydate, lockOnReticleTemplate.heightPlaydate = lockOnReticleTemplate.animations["locked"][1]:getSize()
    lockOnReticleTemplate.angle = 0
    
    return lockOnReticleTemplate
end

function getLockingOnReticle()
  local lockingOnReticleTemplate = {}
    lockingOnReticleTemplate.tVelocity = 0
    lockingOnReticleTemplate.hVelocity = 0
    lockingOnReticleTemplate.dVelocity = 0
    
    lockingOnReticleTemplate.animations = playerAssets.lockingOnReticle.animations
    neonbroom.setState(lockingOnReticleTemplate, "locking")
    
    lockingOnReticleTemplate.widthPlaydate, lockingOnReticleTemplate.heightPlaydate = lockingOnReticleTemplate.animations["locking"][1]:getSize()
    lockingOnReticleTemplate.angle = 0
    
    return lockingOnReticleTemplate
end

function loadPlayerAssets()
  playerAssets = {}
    playerAssets.bullets = {}
    for bulletType, _ in pairs(gunDetails) do
      playerAssets.bullets[bulletType] = {}
        neonbroom.loadAnimation(playerAssets.bullets[bulletType], "flying", "guns/"..bulletType:lower().."bullet", 1, 10)
    end
    
    playerAssets.bulletRadarBlip = {}
      neonbroom.loadAnimation(playerAssets.bulletRadarBlip, "on", "radarblipbullet", 1, 10)
      
    playerAssets.aimingReticle = {}
      neonbroom.loadAnimation(playerAssets.aimingReticle, "aiming", "aimingreticle", 1, 10)
    playerAssets.lockOnReticle = {}
      neonbroom.loadAnimation(playerAssets.lockOnReticle, "locked", "lockonreticle", 1, 10)
    playerAssets.lockingOnReticle = {}
      neonbroom.loadAnimation(playerAssets.lockingOnReticle, "locking", "lockingonreticle", 4, 10)
end

function prepPlayer()
  player = {}
    player.currentFacing = 0
    player.fov = math.pi / 4 + getUpgradeContribution("", "widerfov")
    player.turnRate = math.pi / 2
    
    player.bulletTemplate = getGunBulletTemplate(profile().currentGun)
   
   player.aimingReticle = getAimingReticle()
   player.target = nil
   
   player.lockOnReticle = getLockOnReticle()
   player.lockingOnReticle = getLockingOnReticle()
   
   player.shotCooldownUntil = 0
end
