import "neonbroom"
import "CoreLibs/ui"

menuAnimationTime = 1

local menu = nil

function _fullscreenMenuBackground(name, onLoadFunc)
  bg = {currentOption = 1}
  bg.name = name
  bg.xPosPlaydate = neonbroom.ratios.playdate.width / 2
  bg.yPosPlaydate = neonbroom.ratios.playdate.height / 2
  bg.widthPlaydate = neonbroom.ratios.playdate.width
  bg.heightPlaydate = neonbroom.ratios.playdate.height
  bg.angle = 0
  bg.enabled = true
  bg.onLoad = onLoadFunc
  table.insert(menu, bg)
  
  return bg
end

local crankAlertShowing = false
local crankChangeSinceLastMenuRoll = 0
local lastMenuKeyTime = 0

handleMenuKeypresses = function()
  if (neonbroom.getTimeInSeconds() - menu.animateStart) < menuAnimationTime then return end
  
  --if (neonbroom.getTimeInSeconds() - lastMenuKeyTime) < 0.5 then return end
  
  if (playdate.isCrankDocked()) then
    if crankAlertShowing then playdate.ui.crankIndicator:update()
    else 
      crankAlertShowing = true
      playdate.ui.crankIndicator:start()
    end
  else
    crankAlertShowing = false
  end
  
  local crankChange = playdate.getCrankChange()
  crankChangeSinceLastMenuRoll += crankChange
  if crankChangeSinceLastMenuRoll < -90 then
    _nextMenu(-1)
    crankChangeSinceLastMenuRoll = 0
  elseif crankChangeSinceLastMenuRoll > 90 then
    _nextMenu(1)
    crankChangeSinceLastMenuRoll = 0
  elseif playdate.buttonJustPressed(playdate.kButtonDown) then
    lastMenuKeyTime = neonbroom.getTimeInSeconds()
    _nextMenuOption(1)
  elseif playdate.buttonJustPressed(playdate.kButtonUp) then
    lastMenuKeyTime = neonbroom.getTimeInSeconds()
    _nextMenuOption(-1)
  elseif playdate.buttonJustPressed(playdate.kButtonRight) then
    lastMenuKeyTime = neonbroom.getTimeInSeconds()
    _currentMenuOption().onValueChange(1)
  elseif playdate.buttonJustPressed(playdate.kButtonLeft) then
    lastMenuKeyTime = neonbroom.getTimeInSeconds()
    _currentMenuOption().onValueChange(-1)
  elseif playdate.buttonJustPressed(playdate.kButtonA) then
    lastMenuKeyTime = neonbroom.getTimeInSeconds()
    _currentMenuOption().onExecute()
  end
end

function _nextMenu(direction)
  local priorMenu = menu.currentMenu
  local newMenu = priorMenu + direction
  while (newMenu ~= menu.currentMenu) do
    if (newMenu > #menu) then newMenu = 1 end
    if (newMenu == 0) then newMenu = #menu end
    if menu[newMenu].enabled then
      menu.animateOut = priorMenu
      menu.animateDirection = direction
      menu.animateStart = neonbroom.getTimeInSeconds()
      menu.currentMenu = newMenu
      menu[newMenu].onLoad()
    else
      newMenu = newMenu + direction
    end
  end
end

function _nextMenuOption(direction)
  local priorMenuOption = _currentMenu().currentOption
  local newMenuOption = priorMenuOption + direction
  while (newMenuOption ~= _currentMenu().currentOption) do
    if (newMenuOption > #_currentMenu()) then newMenuOption = 1 end
    if (newMenuOption == 0) then newMenuOption = #_currentMenu() end
    if _currentMenu()[newMenuOption].enabled then
      _currentMenu().currentOption = newMenuOption
      neonbroom.setState(_currentMenu(), _currentMenuOption().bgState)
      _currentMenuOption().onSelect(direction)
    else
      newMenuOption = newMenuOption + direction
    end
  end
end

function _currentMenuOption()
  return _currentMenu()[_currentMenu().currentOption]
end

function _currentMenu() 
  return menu[menu.currentMenu]
end

function _drawMenuWithOffset(obj, offset)
  neonbroom.drawGuiObject(obj, obj.xPosPlaydate + offset)
  
  for i, option in ipairs(obj) do
    for _, child in ipairs(option.children) do
      if (not child.skipDisplay) then
        if (child.moveEnd) then
          if (child.text) then
            -- can't draw the text while it slides for some reason?
          elseif (not child.moveStart or neonbroom.getTimeInSeconds() > child.moveEndTime) then
            neonbroom.drawGuiObject(child, child.moveEnd.x + offset, child.moveEnd.y, child.moveEnd.w, child.moveEnd.h)
          else
            local timePoint = (neonbroom.getTimeInSeconds() - child.moveStartTime) / (child.moveEndTime - child.moveStartTime)
            local x = (child.moveEnd.x - child.moveStart.x) * timePoint + child.moveStart.x+ offset
            local y = (child.moveEnd.y - child.moveStart.y) * timePoint + child.moveStart.y
            local w = (child.moveEnd.w - child.moveStart.w) * timePoint + child.moveStart.w
            local h = (child.moveEnd.h - child.moveStart.h) * timePoint + child.moveStart.h
            neonbroom.drawGuiObject(child, x, y, w, h)
          end
        else
          neonbroom.drawGuiObject(child, child.xPosPlaydate + offset)
        end
      end
    end
    if (i == obj.currentOption) then
      for _, child in ipairs(option.activeChildren) do
        if (not child.skipDisplay) then
          if (child.moveEnd) then
            if (child.text) then
              -- can't draw the text while it slides for some reason?
            elseif (not child.moveStart or neonbroom.getTimeInSeconds() > child.moveEndTime) then
              neonbroom.drawGuiObject(child, child.moveEnd.x + offset, child.moveEnd.y, child.moveEnd.w, child.moveEnd.h)
            else
              local timePoint = (neonbroom.getTimeInSeconds() - child.moveStartTime) / (child.moveEndTime - child.moveStartTime)
              local x = (child.moveEnd.x - child.moveStart.x) * timePoint + child.moveStart.x+ offset
              local y = (child.moveEnd.y - child.moveStart.y) * timePoint + child.moveStart.y
              local w = (child.moveEnd.w - child.moveStart.w) * timePoint + child.moveStart.w
              local h = (child.moveEnd.h - child.moveStart.h) * timePoint + child.moveStart.h
              neonbroom.drawGuiObject(child, x, y, w, h)
            end
          else
            neonbroom.drawGuiObject(child, child.xPosPlaydate + offset)
          end
        end
      end
    end        
  end
end

function isMenuLoaded()
  return true
end

function _drawMenu()
  local animationPercentDone = (neonbroom.getTimeInSeconds() - menu.animateStart) / menuAnimationTime
  if (animationPercentDone < 1 and menu.animateDirection) then
    _drawMenuWithOffset(menu[menu.animateOut], -1 * menu.animateDirection * neonbroom.ratios.playdate.width * animationPercentDone)
    _drawMenuWithOffset(menu[menu.currentMenu], menu.animateDirection * neonbroom.ratios.playdate.width * (1 - animationPercentDone))
  else
    _drawMenuWithOffset(menu[menu.currentMenu], 0)
  end
end

function createWeaponSelectionGuiElement(x, y)
  element = {}
  element.name="weaponSelect"
  
  for _, gunType in ipairs(gunTypes) do
    neonbroom.loadAnimation(element, gunType, "guns/"..gunType:lower().."weaponselection", 1, 10)
  end
  
  element.xPosPlaydate = x
  element.yPosPlaydate = y
  element.widthPlaydate, element.heightPlaydate = element.animations["peaGun"][1]:getSize()
  element.angle = 0
  element.enabled = true
  
  return element
end

function _addMenuState(bg, stateName, animationBase, animationFrames, animatinoFps, childElements, activeChildElements, actionHandlerFunc, valueChangeFunc, onSelectFunc)
  if (not bg.animations or not bg.animations[stateName]) then neonbroom.loadAnimation(bg, stateName, animationBase, animationFrames, animatinoFps) end
  
  local option = {name=bg.name.."_state_"..stateName, bgState=stateName, enabled=(#childElements==0 or childElements[1].enabled), onExecute=actionHandlerFunc, onValueChange=valueChangeFunc, onSelect=onSelectFunc, children=childElements, activeChildren=activeChildElements}
  
  table.insert(bg, option)
end

function _updateResearchValue(obj, direction)
  local maxSeeds = profile().seeds[obj.seedName] or 0
  local currentSeeds = profile().research[obj.seedName] or 0
  local maxTotalSeeds = 15
  local minTotalSeeds = 3
  
  -- todo check total seeds
  
  local newCurrentSeeds = currentSeeds + direction
  if (newCurrentSeeds < 0 or newCurrentSeeds > maxSeeds) then return end
  
  profile().research[obj.seedName] = newCurrentSeeds
  neonbroom.setState(obj, ""..newCurrentSeeds)
  saveProfiles()
end

function _updateSelectedWeapon(obj, direction)
  local priorSelectedGun = gunIndexLookup[profile().currentGun]
  local newSelectedGun = priorSelectedGun + direction
  while (newSelectedGun ~= gunIndexLookup[profile().currentGun]) do
    if (newSelectedGun > #gunTypes) then newSelectedGun = 1 end
    if (newSelectedGun == 0) then newSelectedGun = #gunTypes end
    if profile().isUnlocked[gunTypes[newSelectedGun]] then
      profile().currentGun = gunTypes[newSelectedGun]
      neonbroom.setState(obj, gunTypes[newSelectedGun])
      
      saveProfiles()
    else
      newSelectedGun = newSelectedGun + direction
    end
  end
end

function createResearchPlanningGuiElement(plantType)
  local description = plantDetails[plantType]
  local element = {}
    neonbroom.loadAnimation(element, "purchaseable", "menu/research/"..plantType, 1, 10)
    element.angle = 0
    element.checkEnabled = function() return (profile().seeds[plantType] or 0) > 0 end
    element.enabled = element.checkEnabled()
    element.plantType = plantType
    element.name = plantType
    element.skipDisplay = true
    
  local nameElement = {}
    nameElement.xPosPlaydate = 234
    nameElement.yPosPlaydate = 115
    nameElement.widthPlaydate = 284
    nameElement.heightPlaydate = 31
    nameElement.angle = 0
    nameElement.name = plantType.."_name"
    nameElement.text = description.name
  
  local descriptionElement = {}
    descriptionElement.xPosPlaydate = 234
    descriptionElement.yPosPlaydate = 186
    descriptionElement.widthPlaydate = 284
    descriptionElement.heightPlaydate = 119
    descriptionElement.angle = 0
    descriptionElement.name = plantType.."_desc"
    descriptionElement.text = description.description
    
  local researchPlan = {}
    researchPlan.xPosPlaydate = 350
    researchPlan.yPosPlaydate = 115
    researchPlan.widthPlaydate = 100
    researchPlan.heightPlaydate = 31
    researchPlan.angle = 0
    researchPlan.name = plantType.."_plan"
  
  element.selectedOnlyChildren = {nameElement, descriptionElement, researchPlan}
  element.onWheelUpdate = function()
    element.skipDisplay = ((profile().seeds[plantType] or 0) > 0)
    
    local total = 0
    for _, v in pairs(profile().research) do
      total = total + v
    end
    
    researchPlan.text = (profile().research[plantType] or 0).."/"..(profile().seeds[plantType] or 0).." ("..total.."/15)"
  end

  return element
end

function createStoreUpgradeGuiElement(upgrade)
  local description = upgradeDetails[upgrade]
  local element = {}
    neonbroom.loadAnimation(element, "purchaseable", "upgradeicons/"..upgrade, 1, 10)
    neonbroom.loadAnimation(element, "unpurchaseable", "upgradeicons/"..upgrade.."grayed", 1, 10)
    element.angle = 0
    element.checkEnabled = function() return not description.unlock or profile().isUnlocked[description.unlock] end
    element.enabled = element.checkEnabled()
    element.upgrade = upgrade
    element.name = upgrade
    element.skipDisplay = true
    
  local nameElement = {}
    nameElement.xPosPlaydate = 234
    nameElement.yPosPlaydate = 115
    nameElement.widthPlaydate = 284
    nameElement.heightPlaydate = 31
    nameElement.angle = 0
    nameElement.name = upgrade.."_name"
    nameElement.text = description.name
  
  local descriptionElement = {}
    descriptionElement.xPosPlaydate = 234
    descriptionElement.yPosPlaydate = 176
    descriptionElement.widthPlaydate = 284
    descriptionElement.heightPlaydate = 99
    descriptionElement.angle = 0
    descriptionElement.name = upgrade.."_desc"
    descriptionElement.text = description.description
    
  local costElement = {}
    costElement.xPosPlaydate = 350
    costElement.yPosPlaydate = 216
    costElement.widthPlaydate = 284
    costElement.heightPlaydate = 31
    costElement.name = upgrade.."_cost"
    costElement.angle = 0
    element.costElement = costElement
    
  local levelElement = {}
    levelElement.xPosPlaydate = 234
    levelElement.yPosPlaydate = 216
    levelElement.widthPlaydate = 284
    levelElement.heightPlaydate = 31
    levelElement.angle = 0
    levelElement.name = upgrade.."_level"
    element.levelElement = levelElement
  
  element.selectedOnlyChildren = {nameElement, descriptionElement, costElement, levelElement}
  element.onWheelUpdate = function()
    updateStoreUpgradeGuiElementState(element)
    updateStoreDynamicText(element)
  end

  return element
end

function updateMenuWheel(wheel, direction)
  local ctEnabled = 0
  for i,v in ipairs(wheel) do
    local c = v.children[1]
    c.enabled = c.checkEnabled()
    v.enabled = c.enabled
    if (v.enabled) then ctEnabled = ctEnabled + 1 end
  end

  local optionIndex = wheel.currentOption
  local oneUp, oneDown = nil, nil
  if (ctEnabled >= 4) then
    oneUp = optionIndex - 1
    if (oneUp < 1) then oneUp = oneUp + #wheel end
    while (not wheel[oneUp].enabled) do
      oneUp = oneUp - 1
      if (oneUp < 1) then oneUp = oneUp + #wheel end
    end
      
    oneDown = optionIndex + 1
    if (oneDown > #upgrades) then oneDown = oneDown - #wheel end
    while (not wheel[oneDown].enabled) do
      oneDown = oneDown + 1
      if (oneDown > #upgrades) then oneDown = oneDown - #wheel end
    end
  end
  
  local popUp =      {x = 44, y = 86,  w = 0,  h = 0}
  local oneUpPos =   {x = 44, y = 104, w = 32, h = 32}
  local centerPos =  {x = 44, y = 154, w = 60, h = 60}
  local oneDownPos = {x = 44, y = 204, w = 32, h = 32}
  local popDown =    {x = 44, y = 222, w = 0,  h = 0}

  local animationTime = 0.2
  for i,v in ipairs(wheel) do
    v = v.children[1]
    if (i == oneUp) then
      v.onWheelUpdate()
      v.skipDisplay = false
      v.moveStart = (not v.movedOff and v.moveEnd) or direction == 1 and popDown or direction == -1 and popUp or oneUpPos
      v.moveEnd = oneUpPos
      v.movedOff = false
      v.moveStartTime = neonbroom.getTimeInSeconds()
      v.moveEndTime = neonbroom.getTimeInSeconds() + animationTime
    elseif (i == optionIndex) then
      v.onWheelUpdate()
      v.skipDisplay = false
      v.moveStart = (not v.movedOff and v.moveEnd) or direction == 1 and popDown or direction == -1 and popUp or centerPos
      v.moveEnd = centerPos
      v.movedOff = false
      v.moveStartTime = neonbroom.getTimeInSeconds()
      v.moveEndTime = neonbroom.getTimeInSeconds() + animationTime
    elseif (i == oneDown) then
      v.onWheelUpdate()
      v.skipDisplay = false
      v.moveStart = (not v.movedOff and v.moveEnd) or direction == 1 and popDown or direction == -1 and popUp or oneDownPos
      v.moveEnd = oneDownPos
      v.movedOff = false
      v.moveStartTime = neonbroom.getTimeInSeconds()
      v.moveEndTime = neonbroom.getTimeInSeconds() + animationTime
    elseif (not v.skipDisplay) then
      v.onWheelUpdate()
      v.skipDisplay = false
      v.moveStart = v.moveEnd
      v.moveEnd = direction == 1 and popUp or direction == -1 and popDown or v.moveStart
      v.movedOff = true
      v.moveStartTime = neonbroom.getTimeInSeconds()
      v.moveEndTime = neonbroom.getTimeInSeconds() + animationTime
    else
      v.skipDisplay = true
      v.moveEnd = nil
      v.movedOff = true
    end
  end
end

function updateStoreUpgradeGuiElementState(element)
  
  if canAffordUpgrade(element.upgrade) and isUpgradeable(element.upgrade) then
    neonbroom.setState(element, "purchaseable")
  else
    neonbroom.setState(element, "unpurchaseable")
  end
end

function updateStoreDynamicText(element)
  
  local currentLevel = profile().upgradeLevels[element.upgrade] or 0
  local maxLevel = upgradeDetails[element.upgrade].maxLevel
  
  local levelText = "not owned"
  if currentLevel > 0 then
    levelText = "lvl "..(currentLevel == maxLevel and "MAX" or currentLevel)
  end
  
  local costText = (profile().wallet[upgradeDetails[element.upgrade].currencyType] or 0).."/"..computeCostToBuyUpgrade(element.upgrade).." "..currencyDetails[upgradeDetails[element.upgrade].currencyType].displayName
  
  element.costElement.text = costText
  element.levelElement.text = levelText
  
end

function updateResearchPlan(researchElement, d)
  local total = 0
  for _, v in pairs(profile().research) do
    total = total + v
  end
  
  if (d > 0 and profile().research[researchElement.plantType] < profile().seeds[researchElement.plantType] and total < 15) then
    profile().research[researchElement.plantType] = profile().research[researchElement.plantType] + 1
  elseif (d < 0 and profile().research[researchElement.plantType] > 0 and total > 3) then
    profile().research[researchElement.plantType] = profile().research[researchElement.plantType] - 1
  end
  
  saveProfiles()
end

function loadMenus()
  menu = {currentMenu = 1, animateStart = 0}
  
  local gunTypeGuiElement = createWeaponSelectionGuiElement(137, 176)
  local play = _fullscreenMenuBackground("play", function() neonbroom.setState(gunTypeGuiElement, profile().currentGun) end)
  _addMenuState(play, "play", "mainmenuplayselected", 1, 10, {}, {}, function() startLevel() end, function(d) end, function(d) end)
  _addMenuState(play, "pickveggie", "mainmenupickveggieselected", 1, 10, {gunTypeGuiElement}, {}, function() end, function(d) _updateSelectedWeapon(gunTypeGuiElement, d) end, function(d) end)
  
  researchMenuPage = _fullscreenMenuBackground("research", function() updateMenuWheel(researchMenuPage, 0) end)
  for i, plantType in ipairs(plantTypes) do
    local researchElement = createResearchPlanningGuiElement(plantType)
    _addMenuState(researchMenuPage, "store", "mainmenustore", 1, 10, {researchElement}, researchElement.selectedOnlyChildren, function() end, function(d) updateResearchPlan(researchElement, d); updateMenuWheel(researchMenuPage, 0) end, function(d) updateMenuWheel(researchMenuPage, d) end)
  end

  storeMenuPage = _fullscreenMenuBackground("store", function() updateMenuWheel(storeMenuPage, 0) end)
  for i, upgrade in ipairs(upgrades) do
    local upgradeElement = createStoreUpgradeGuiElement(upgrade)
    _addMenuState(storeMenuPage, "store", "mainmenustore", 1, 10, {upgradeElement}, upgradeElement.selectedOnlyChildren, function() purchaseUpgrade(upgrade); updateMenuWheel(storeMenuPage, 0) end, function(d) end, function(d) updateMenuWheel(storeMenuPage, d) end)
  end
  
  local preferences = {}  
  
  menu[menu.currentMenu].onLoad()
end

loadMenus()