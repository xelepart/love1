import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"
import "TSerial"

local gfx <const> = playdate.graphics

neonbroom = {
  
  -- a couple love-to-playdate temp helpers
  --(image, xPos, yPos, obj.angle, width*imageXScale, height*imageYScale, image:getWidth() / 2, image:getHeight() / 2)
  oldDraw = function(image,x,y,angle,xSize, ySize, xOffset, yOffset)
    local w,h = image:getSize()
    print(w..","..h..","..x..","..y..","..xSize..","..ySize..","..xOffset..","..yOffset)
  end,
  
  currentTime=0,
  setUpdateTime = function(newTime)
    neonbroom.currentTime = newTime
  end,
  getTimeInSeconds = function()
    return neonbroom.currentTime / 1000.0
  end,
  
  --Defining all our global constants I guess
  
  screenAspectRatio = 400.0 / 240.0,
  
  log = function(str)
    print(str)
  end,

  removeFlagged = function(array, cleanUp)
    local j = 1
    local n = #array -- "length of array"
    
    for i = 1, n do
      if (array[i] and array[i].remove) then
        if cleanUp then array[i].remove = nil end
        array[i] = nil
      else
        if (i ~= j) then
          array[j] = array[i]
          array[i] = nil
        end
        j = j + 1 -- Increment position of where we'll place the next kept value.
      end
    end
  end,
  
  --gets the coordinates of a GUI object (aiming reticle or HUD or whatever) for scaling purposes
  getGuiCoords = function(obj, xOverride, yOverride, wOverride, hOverride)
    local xPos = (xOverride or obj.xPosPlaydate) * neonbroom.uiScale
    local yPos = (yOverride or obj.yPosPlaydate) * neonbroom.uiScale
    local width = (wOverride or obj.widthPlaydate) * neonbroom.uiScale
    local height = (hOverride or obj.heightPlaydate) * neonbroom.uiScale
    
    return xPos + neonbroom.screenShakeXOffset, yPos + neonbroom.screenShakeYOffset, width, height
  end,

  --draws the GUI object after stretching it to fit the aspect ratio
  drawGuiObject = function(obj, xOverride, yOverride, wOverride, hOverride)
    local xPos, yPos, width, height = neonbroom.getGuiCoords(obj, xOverride, yOverride, wOverride, hOverride)
    
    if (width < 1 or height < 1) then return end
    
    if (obj.text) then
      gfx.drawTextInRect(obj.text, xPos-width/2, yPos-height/2, width, height, 0)
    else
      local image = neonbroom.getImage(obj)
      local w,h = image:getSize()
      local scale = width/w
      image:drawRotated(xPos, yPos, (obj.angle * 360 / (2*math.pi) or 0), scale)
    end
  end,
  
  --translates the coordinates and size of an object in the game from polar coordinates (for game math purposes) to cartesian coordinates (for screen math purposes)
  getObjectScreenCoords = function(obj)
    return neonbroom.getObjectSimulatedScreenCoordsOptAngle(obj, player.currentFacing, true, 0)
  end,
  
  getObjectSimulatedScreenCoords = function(obj, centerTheta)
    return neonbroom.getObjectSimulatedScreenCoordsOptAngle(obj, centerTheta, false, 0)
  end,
  
  getObjectSimulatedScreenCoordsOptAngle = function(obj, centerTheta, computeAngle, dt)
    local relativeTheta = ((obj.theta + dt*(obj.tVelocity or 0)) - centerTheta + math.pi) % (2 * math.pi) - math.pi
    
    local angleFromTurretToObject = relativeTheta
    local distanceFromTurretToObject = obj.distance + dt*(obj.dVelocity or 0)
    
    local angleFromBehindTurretToObject = math.pi - relativeTheta
    
    local distanceFromCameraToObject = math.sqrt(neonbroom.cameraDistanceBehindTurret^2 + distanceFromTurretToObject^2 - 2*neonbroom.cameraDistanceBehindTurret*distanceFromTurretToObject*math.cos(angleFromBehindTurretToObject))
    local angleFromCameraToObject = math.asin(math.sin(angleFromBehindTurretToObject) / distanceFromCameraToObject * distanceFromTurretToObject)
    
    local scaledTheta = (angleFromCameraToObject + (player.fov / 2)) / player.fov
    
    local xPos = scaledTheta * windowWidth * neonbroom.ratios.playdate.playAreaWidthScale
    local xPosWithShake = xPos + neonbroom.screenShakeXOffset * (windowWidth / neonbroom.ratios.playdate.width)
    
    local distanceScale = (((maxDistance + neonbroom.cameraDistanceBehindTurret) - (distanceFromCameraToObject)) / (maxDistance + neonbroom.cameraDistanceBehindTurret))
    
    local focalHeight = 50
    local horizonHeight = viewport.height / 3
    local objHeightRelative = (obj.height + dt*(obj.hVelocity or 0)) - focalHeight / viewport.height
    
    local focalHeightPlaydate = 100
    local minFocalRangePlaydate = 60
    local dynamicFocalRangePlaydate = 40
    
    local objHeightPlaydate = (obj.height + dt*(obj.hVelocity or 0)) * 2
    local objFocalHeightOffset = (objHeightPlaydate - focalHeightPlaydate) / 100 -- -1 to 1
    local yPos = (focalHeightPlaydate + (minFocalRangePlaydate + dynamicFocalRangePlaydate * distanceScale) * objFocalHeightOffset) * neonbroom.uiScale
    local yPosWithShake = yPos + neonbroom.screenShakeYOffset * (windowHeight / neonbroom.ratios.playdate.height)
    
    local sizeScale = obj.size / viewport.height
    local actualScale = math.max(sizeScale * math.min(math.sqrt(distanceScale) + 0.1, 1) * windowHeight, 10)

    local angle = 0
    if (computeAngle and obj.usesDirectionalRotation) then
      local nextX, nextY, _, _ = neonbroom.getObjectSimulatedScreenCoordsOptAngle(obj, centerTheta, false, 0.01)
      if (math.abs(nextX - xPosWithShake) < .001) then angle = 0 else
        angle = math.atan2(nextY - yPosWithShake, nextX - xPosWithShake) + math.pi/2
      end
    end
  
    return xPosWithShake, yPosWithShake, actualScale, angle
  end,
    
  --draws a thing on screen given its cartesian coordinates and size
  drawPhysicalObject = function(obj)
    local xPos, yPos, scale, angle = neonbroom.getObjectScreenCoords(obj)

    scale = math.max(2, scale * (obj.scale or 1))
    local image = neonbroom.getImage(obj)
    
    local w,h = image:getSize();
    local imageScale = 1 / h
    local relativeTheta = (obj.theta - player.currentFacing + math.pi) % (2 * math.pi) - math.pi

    if (relativeTheta > -player.fov and relativeTheta < player.fov and obj.distance > 1) then
      image:drawScaled(xPos-w*scale*imageScale/2,yPos-h*scale*imageScale/2,1)
    end
    
    if (obj.lockOnStartTime and obj.lockOnStartTime + player.bulletTemplate.lockOnTime >= neonbroom.getTimeInSeconds()) then
      local lockingOnReticle = player.lockingOnReticle
      local reticleImage = neonbroom.getImage(lockingOnReticle)
      local riw, rih = reticleImage:getSize()
      local reticleScale = 1 / rih
      reticleImage:drawScaled(xPos-riw*scale*2*reticleScale/2,yPos-rih*scale*2*reticleScale/2,scale*2*reticleScale)
    end
  end
}

neonbroom.shakeStepsRemaining = 0
neonbroom.lastShakeUpdate = 0
neonbroom.screenShakeXOffset = 0
neonbroom.screenShakeYOffset = 0

neonbroom.setState = function(obj, state)
  obj.lastSetTime = neonbroom.getTimeInSeconds()
  if (obj.state == state) then return end
  
  if (false and (obj.state == "posteating" or state == "posteating")) then
    print(obj.state.."->"..state..": "..debug.traceback())
  end

  obj.state = state
  obj.stateStartTime = neonbroom.getTimeInSeconds()
end

neonbroom.getImage = function(obj)
  if obj.image then return obj.image end
  
  if obj.images then return obj.images[obj.imageArrayPos] end
  
  if (not obj.animations[obj.state]) then
    print(obj.name)
    print(obj.state)
  end
  
  if (obj.animations[obj.state].noRepeat) then
    return obj.animations[obj.state][math.min(math.floor((neonbroom.getTimeInSeconds() - obj.stateStartTime) * obj.animations[obj.state].fps) + 1, #obj.animations[obj.state])]
  else
    return obj.animations[obj.state][math.floor((neonbroom.getTimeInSeconds() - obj.stateStartTime) * obj.animations[obj.state].fps) % #obj.animations[obj.state] + 1]
  end
end

neonbroom.getAnimationRepeatCount = function(obj)    
  return math.floor((neonbroom.getTimeInSeconds() - obj.stateStartTime) * obj.animations[obj.state].fps) / #obj.animations[obj.state]
end

neonbroom.loadAnimation = function(obj, state, rootImageFilepath, imageCount, animationFps, noRepeat)
  obj.animations = obj.animations or {}
  local newState = {}
    newState.fps = animationFps
  if imageCount > 10000 then print("ERROR: STATE " + state + " HAS > 10000 IMAGES AND WE DON'T DO THAT.") end
  for i = 1, imageCount do
    local filename
    if i < 11 then
      filename = "images/"..rootImageFilepath.."000"..(i-1)..""
    elseif i < 101 then
      filename = "images/"..rootImageFilepath.."00"..(i-1)..""
    elseif i < 1001 then
      filename = "images/"..rootImageFilepath.."0"..(i-1)..""
    else
      filename = "images/"..rootImageFilepath..""..(i-1)..""
    end
    local newImage = gfx.image.new(filename)
    if (newImage) then newState[i] = newImage end
  end
  if state == "happy" or noRepeat then newState.noRepeat = true end
  
  if (#newState > 0) then obj.animations[state] = newState end
  if not obj.state and #newState > 0 then neonbroom.setState(obj, state) end
end

neonbroom.thetaDiff = function(theta1, theta2)
  while (theta1 < -math.pi) do theta1 = theta1 + 2 * math.pi end
  while (theta2 < -math.pi) do theta2 = theta2 + 2 * math.pi end
  while (theta1 > math.pi) do theta1 = theta1 - 2 * math.pi end
  while (theta2 > math.pi) do theta2 = theta2 - 2 * math.pi end
  
  return math.abs(theta2 - theta1)
end

function thetaDiff2(theta1, theta2)
  local thetaDiff = theta2 - theta1
  while (thetaDiff > math.pi) do thetaDiff = thetaDiff - 2*math.pi end
  while (thetaDiff <= -math.pi) do thetaDiff = thetaDiff + 2*math.pi end
  return thetaDiff
end

neonbroom.updateVelocityFromTo = function(obj1, obj2)
  if (not obj2) then return end
  
  if (obj1.state == "disappearing" or obj1.state == "reappearing") then return end
  
  local targetTheta = obj2.theta
  local targetDistance = obj2.distance
  local targetHeight = obj2.height
  
  if (obj2.remove or obj2.state == "happy") then
    targetTheta = obj1.theta
    targetDistance = 100
    targetHeight = obj1.height
  end
  
  if (obj1.avoidsFov) then
    local thetaDiff = thetaDiff2(obj1.theta, player.currentFacing) 
    local thetaDiffToTarget = thetaDiff2(obj1.theta, obj1.targetPlant.theta)
    
    local passingThroughFOVCW = (thetaDiff > 0 and thetaDiffToTarget > 0 and thetaDiff < thetaDiffToTarget + player.fov / 2) 
    local passingThroughFOVCCW = (thetaDiff < 0 and thetaDiffToTarget < 0 and thetaDiff > thetaDiffToTarget - player.fov / 2)
    
    local state = obj1.state
    local timeInState = neonbroom.getTimeInSeconds() - obj1.stateStartTime
    local timeSinceStateSet = neonbroom.getTimeInSeconds() - obj1.lastSetTime
    local isEating = (state == "eating" and (timeSinceStateSet < (obj1.chewTime or 0)))
    local inFOV = math.abs(thetaDiff) < player.fov * 0.55
    local isSurprised = inFOV and (state == "leftsurprised" or state == "rightsurprised") and (timeInState < 1)
    local dir = (state == "fleeright" or state == "rightsurprised" or ((state == "preeating" or state == "eating" or state == "posteating") and thetaDiff < 0)) and "right" or "left"
    local feelsSafe = obj1.avoidsFovSafe
    
    obj1.targetable = true
    if (isEating) then
      targetDistance = obj1.distance
      targetHeight = obj1.height
      targetTheta = obj1.theta
    elseif inFOV and feelsSafe then 
      targetDistance = obj1.distance
      targetHeight = obj1.height
      targetTheta = obj1.theta
      neonbroom.setState(obj1, dir.."surprised")
    elseif inFOV and isSurprised then
      targetDistance = obj1.distance
      targetHeight = obj1.height
      targetTheta = obj1.theta
    elseif inFOV then
      targetDistance = obj1.distance
      targetHeight = obj1.height
      targetTheta = obj1.theta - (thetaDiff / math.abs(thetaDiff)) * player.fov * 1.1
      neonbroom.setState(obj1, "flee"..(thetaDiff < 0 and "right" or "left"))
    else
      -- "normal" movement, but don't pass through the FOV:
      obj1.targetable = false
      if passingThroughFOVCW then targetTheta = player.currentFacing - player.fov * 0.58; neonbroom.setState(obj1, "fleeright")
      elseif passingThroughFOVCCW then targetTheta = player.currentFacing + player.fov * 0.58; neonbroom.setState(obj1, "fleeleft")
      else neonbroom.setState(obj1, "flee"..dir) end
    end
    
    obj1.avoidsFovSafe = isEating or not inFOV
  end
  
  if (not targetTheta or not targetDistance or not targetHeight) then return end
  
  if (obj1.fliesStraight) then
    local sx, sy, sz = polarToCartesian(obj1.theta, obj1.height, obj1.distance)
    obj1.xPos, obj1.yPos, obj1.zPos = sx, sy, sz
    local ex, ey, ez = polarToCartesian(targetTheta, targetHeight, targetDistance)
    local dx, dy, dz = ex-sx, ey-sy, ez-sz
    if (math.abs(dx) + math.abs(dz)) >= 5 then dy = dy - (obj1.goalHeightOffset or 0) end
    local hypo = math.sqrt(dx^2 + dy^2 + dz^2)
    obj1.xVelocity, obj1.yVelocity, obj1.zVelocity = dx/hypo*obj1.speed, dy/hypo*obj1.speed, dz/hypo*obj1.speed
  else
    local thetaDiff = targetTheta - obj1.theta
    while (thetaDiff < -math.pi) do thetaDiff = thetaDiff + 2*math.pi end
    while (thetaDiff > math.pi) do thetaDiff = thetaDiff - 2*math.pi end
    
    local distanceOffset = (obj1.shieldTarget == obj2) and obj1.goalDistanceOffset or 0
    local distanceDiff = (targetDistance + distanceOffset) - obj1.distance
    local travelDistanceDistance = math.abs(distanceDiff)
    
    local heightOffset = obj1.goalHeightOffset or 0
    
    local avgObjDistance = (obj1.distance + targetDistance) / 2
    local travelArcLength = avgObjDistance * thetaDiff
    local travelArcDistance = math.abs(travelArcLength)

    if travelArcDistance + travelDistanceDistance < 5 then
      heightOffset = 0
    end
      
    local heightDiff = targetHeight - heightOffset - obj1.height
    if (obj1.distance > 50 and distanceDiff > 30) then targetHeight = obj1.height end
    local travelHeightDistance = math.abs(heightDiff)
    
    local totalDistanceTraveled = travelArcDistance + travelHeightDistance + travelDistanceDistance
    
    if (obj1.speedsUpIfNecessary and obj1.lastTotalDistanceTraveled and totalDistanceTraveled > obj1.lastTotalDistanceTraveled) then
      obj1.speed = obj1.speed * 1.01
    end
    
    if (obj1.state == "preeating" and (neonbroom.getTimeInSeconds() - obj1.lastSetTime) >= (obj1.preEatTime or 0)) then
      neonbroom.setState(obj1, "eating")
    elseif (obj1.state == "eating" and (neonbroom.getTimeInSeconds() - obj1.lastSetTime) >= (obj1.chewTime or 0)) then
      if (obj1.postEatTime or 0 > 0) then --nsr
        neonbroom.setState(obj1, "posteating")
      else
        neonbroom.setState(obj1, "forward")
      end
    elseif (obj1.state == "posteating" and (neonbroom.getTimeInSeconds() - obj1.lastSetTime) >= (obj1.postEatTime or 0)) then
      neonbroom.setState(obj1, "forward")
    end
 
    if obj1.state == "preeating" or obj1.state == "eating" or obj1.state == "posteating" then
      obj1.tVelocity = 0
      obj1.hVelocity = 0
      obj1.dVelocity = 0
    elseif obj1.shieldedBy and not obj1.shieldedBy.remove and (obj1.shieldedBy.state == "eating" --[[or obj1.shieldedBy.state == "happy"]]) then
      obj1.tVelocity = 0
      obj1.hVelocity = 0
      obj1.dVelocity = 0
    elseif totalDistanceTraveled < 1 and obj1.state ~= "flying" then
      obj1.tVelocity = 0
      obj1.hVelocity = 0
      obj1.dVelocity = 0
      if (obj1.animations.left) then neonbroom.setState(obj1, "left") end
    else
      obj1.tVelocity = travelArcLength / totalDistanceTraveled * obj1.speed / avgObjDistance
      obj1.hVelocity = heightDiff / totalDistanceTraveled * obj1.speed
      obj1.dVelocity = distanceDiff / totalDistanceTraveled * obj1.speed
      
      if (obj1.shieldTarget == obj2 and totalDistanceTraveled < 3) then
        neonbroom.setState(obj1, "forward")
      elseif obj1.hasDirectionlessFlight then
        neonbroom.setState(obj1, "forward")
      elseif obj1.state ~= "flying" and obj1.animations.left then
        if (travelArcLength / totalDistanceTraveled < -0.5) then neonbroom.setState(obj1, "hardleft")
        elseif (travelArcLength / totalDistanceTraveled < 0) then neonbroom.setState(obj1, "left")
        elseif (travelArcLength / totalDistanceTraveled < 0.5) then neonbroom.setState(obj1, "right")
        else neonbroom.setState(obj1, "hardright")
        end
      end
    end
    
    obj1.lastTotalDistanceTraveled = totalDistanceTraveled
  end
end

neonbroom.setCurrentLevel = function(levelId)
  if (type(levelId) == "number") then levelId = "level" .. levelId end
  if (currentLevelDefinition ~= levelDefinitions[levelId]) then
    currentLevelDefinition = levelDefinitions[levelId]
    currentLevelDefinition.loadLevel()
  end
end

neonbroom.handleMenuKeypress = function(menuObj, key)
  if key == "down" or key == "s" then
    neonbroom.nextMenuOption(menuObj, 1)
  elseif key == "up" or key == "w" then
    neonbroom.nextMenuOption(menuObj, -1)
  elseif key == "space" then
    menuObj.options[menuObj.selectedOption].onExecute()
  end
end

neonbroom.nextMenuOption = function(menuObj, direction)
  local oldSelectedOption = menuObj.selectedOption
  local newSelectedOption = oldSelectedOption + direction
  while (newSelectedOption ~= menuObj.selectedOption) do
    if (newSelectedOption > #menuObj.options) then newSelectedOption = 1 end
    if (newSelectedOption == 0) then newSelectedOption = #menuObj.options end
    if menuObj.options[newSelectedOption].enabled then
      neonbroom.setState(menuObj.options[oldSelectedOption], "unselected")
      neonbroom.setState(menuObj.options[newSelectedOption], "selected")
      menuObj.selectedOption = newSelectedOption
    else
      newSelectedOption = newSelectedOption + direction
    end
  end
end

neonbroom.invertITable = function(t)
  local t2 = {}
    for i,v in ipairs(t) do
      t2[v] = i
    end
  return t2;
end

--defining Playdate constants
neonbroom.uiScale = 1
neonbroom.cameraDistanceBehindTurret = 10
neonbroom.ratios = {}
  neonbroom.ratios.playdate = {}
    neonbroom.ratios.playdate.playAreaWidth = 320
    neonbroom.ratios.playdate.playAreaHeight = 200
    neonbroom.ratios.playdate.focalPointHeight = 100
    neonbroom.ratios.playdate.width = 400
    neonbroom.ratios.playdate.height = 240
    neonbroom.ratios.playdate.playAreaWidthScale = neonbroom.ratios.playdate.playAreaWidth / neonbroom.ratios.playdate.width
    neonbroom.ratios.playdate.playAreaHeightScale = neonbroom.ratios.playdate.playAreaHeight / neonbroom.ratios.playdate.height

--below this is all GUI stuff
neonbroom.hudBase = {}
  neonbroom.hudBase.xPosPlaydate = 360
  neonbroom.hudBase.yPosPlaydate = 120
  neonbroom.hudBase.image = gfx.image.new("images/hudmockup")
  neonbroom.hudBase.widthPlaydate, neonbroom.hudBase.heightPlaydate = neonbroom.hudBase.image:getSize()
  neonbroom.hudBase.angle = 0
  
neonbroom.radarBackground = {}
  neonbroom.radarBackground.xPosPlaydate = 360
  neonbroom.radarBackground.yPosPlaydate = 40
  neonbroom.radarBackground.image = gfx.image.new("images/radardonut")
  neonbroom.radarBackground.widthPlaydate, neonbroom.radarBackground.heightPlaydate = neonbroom.radarBackground.image:getSize()
  neonbroom.radarBackground.angle = 0
  
neonbroom.radarFovLeft = {}
  neonbroom.radarFovLeft.xPosPlaydate = neonbroom.radarBackground.xPosPlaydate
  neonbroom.radarFovLeft.yPosPlaydate = neonbroom.radarBackground.yPosPlaydate
  neonbroom.radarFovLeft.image = gfx.image.new("images/radarleftline")
  neonbroom.radarFovLeft.widthPlaydate, neonbroom.radarFovLeft.heightPlaydate = neonbroom.radarFovLeft.image:getSize()
  neonbroom.radarFovLeft.angle = 0
  
neonbroom.radarFovRight = {}
  neonbroom.radarFovRight.xPosPlaydate = neonbroom.radarBackground.xPosPlaydate
  neonbroom.radarFovRight.yPosPlaydate = neonbroom.radarBackground.yPosPlaydate
  neonbroom.radarFovRight.image = gfx.image.new("images/radarrightline")
  neonbroom.radarFovRight.widthPlaydate, neonbroom.radarFovRight.heightPlaydate = neonbroom.radarFovRight.image:getSize()
  neonbroom.radarFovRight.angle = 0
  
neonbroom.healthBar = {}
  neonbroom.healthBar.xPosPlaydate = 360
  neonbroom.healthBar.yPosPlaydate = 90
  neonbroom.healthBar.images = {}  --loading 61 separate health bar images into an array, numbered from 0 (empty) to 60 (full)
    for i = 1, 61 do
      if i < 11 then  --health bar image names start at 00 for better alphabetization, so this if statement helps put them into the array in numerical order
        neonbroom.healthBar.images[i] = gfx.image.new("images/healthbar/healthbar0"..(i-1).."px")
      else
        neonbroom.healthBar.images[i] = gfx.image.new("images/healthbar/healthbar"..(i-1).."px")
      end
    end
  neonbroom.healthBar.widthPlaydate,neonbroom.healthBar.heightPlaydate = neonbroom.healthBar.images[1]:getSize()
  neonbroom.healthBar.angle = 0