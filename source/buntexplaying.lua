import "neonbroom"
import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"

local gfx <const> = playdate.graphics


local startTime = 0
function startLevel()
  startTime = neonbroom.getTimeInSeconds()
  currentLevelDefinition.resetLevel()
  prepPlayer()
  gameState.currentState = "playing"
end

function distanceSquaredBetweenPhysicalPoints(obj1, obj2)
  local avgDistance = (obj1.distance + obj2.distance) / 2
  local thetaDiff = obj2.theta - obj1.theta
  while (thetaDiff < -math.pi) do thetaDiff = thetaDiff + 2*math.pi end
  while (thetaDiff > math.pi) do thetaDiff = thetaDiff - 2*math.pi end
  
  return (thetaDiff * avgDistance)^2 + (obj2.height - obj1.height)^2 + (obj2.distance - obj1.distance)^2
end

function cartesianToPolar(xPos, yPos, zPos)
  local distance = math.sqrt(xPos^2 + zPos^2)
  local height = yPos
  local theta = -math.atan2(zPos, xPos) + math.pi/2
  
  return theta, height, distance
end

function polarToCartesian(theta, height, distance)
  local xPos = math.cos(math.pi/2 - theta) * distance
  local yPos = height
  local zPos = math.sin(math.pi/2 - theta) * distance
  
  if (math.abs(xPos) < 0.000001) then xPos = 0 end
  if (math.abs(zPos) < 0.000001) then zPos = 0 end
  return xPos, yPos, zPos
end

updatePlaying = function(dt)
  local gameTime = neonbroom.getTimeInSeconds()
    -- breaks the game if game over
  if level.ended then
    if math.floor(neonbroom.getTimeInSeconds()) - timePlayed >= 1 then
      canGoBackToMainMenuAfterGameOver = true
    end
    return
  else
    timePlayed = neonbroom.getTimeInSeconds()
  end
    
  --shakes everything if you get hit
  if (neonbroom.shakeStepsRemaining > 0 and neonbroom.lastShakeUpdate < neonbroom.getTimeInSeconds() - 0.05) then
    neonbroom.lastShakeUpdate = neonbroom.getTimeInSeconds()
    neonbroom.shakeStepsRemaining = neonbroom.shakeStepsRemaining - 1
    neonbroom.screenShakeXOffset = (math.random() < 0.5 and -4 or 4)
    neonbroom.screenShakeYOffset = (math.random() < 0.5 and -4 or 4)
  elseif (neonbroom.shakeStepsRemaining == 0) then
    neonbroom.screenShakeXOffset = 0
    neonbroom.screenShakeYOffset = 0
  end  
  
  --turns the crank to rotate the universe
  local crankChange = playdate.getCrankChange()
  if (crankChange ~= 0) then player.currentFacing = player.currentFacing + player.turnRate * crankChange / 720 end
    
  --moves the aiming reticle using keyboard commands
  canMoveRight = player.aimingReticle.xPosPlaydate < neonbroom.ratios.playdate.playAreaWidth - player.aimingReticle.widthPlaydate / 2
  canMoveLeft = player.aimingReticle.xPosPlaydate > 0 + player.aimingReticle.widthPlaydate / 2
  canMoveDown = player.aimingReticle.yPosPlaydate <  neonbroom.ratios.playdate.playAreaHeight - player.aimingReticle.heightPlaydate / 2
  canMoveUp = player.aimingReticle.yPosPlaydate > 0 + player.aimingReticle.heightPlaydate/ 2

  tryingToMoveRight = playdate.buttonIsPressed(playdate.kButtonRight)
  tryingToMoveLeft = playdate.buttonIsPressed(playdate.kButtonLeft)
  tryingToMoveDown = playdate.buttonIsPressed(playdate.kButtonDown)
  tryingToMoveUp = playdate.buttonIsPressed(playdate.kButtonUp)
  
  if tryingToMoveRight and canMoveRight then
    player.aimingReticle.xPosPlaydate = player.aimingReticle.xPosPlaydate + player.aimingReticle.velocity * dt
  end

  if tryingToMoveLeft and canMoveLeft then
    player.aimingReticle.xPosPlaydate = player.aimingReticle.xPosPlaydate - player.aimingReticle.velocity * dt
  end
  
  if tryingToMoveDown and canMoveDown then
    player.aimingReticle.yPosPlaydate = player.aimingReticle.yPosPlaydate + player.aimingReticle.velocity * dt
  end

  if tryingToMoveUp and canMoveUp then
    player.aimingReticle.yPosPlaydate = player.aimingReticle.yPosPlaydate - player.aimingReticle.velocity * dt
  end
  
  -- moves bunnies, checks if they hit the plants
  moveBunniesAndCheckForPlants(level.bunnies, dt)
  if (boss) then moveBunniesAndCheckForPlants(boss.bunnies, dt) end
  
  -- lock on
  local xReticle, yReticle, widthReticle, heightReticle = neonbroom.getGuiCoords(player.aimingReticle)
  
  player.target = nil
  local maxReticleDistanceToTarget = (player.aimingReticle.lockOnRange * neonbroom.uiScale) ^ 2
  local maxTargetsToCycle = player.aimingReticle.maxTargetsToCycle
  local numTargetsCycled = 0
  
  local bunniesLists = {level.bunnies}
  if (boss) then
    table.insert(bunniesLists, boss.bunnies)
    table.insert(bunniesLists, {boss})
  end
  
  for _,bunnies in ipairs(bunniesLists) do
    for i, bunny in ipairs(bunnies) do
      local targetable = bunny.targetable
      
      if (targetable) then
        if (bunny.shieldedBy and bunny.shieldedBy.hp > 0) then
          local bunnySize = (1 - (bunny.distance + neonbroom.cameraDistanceBehindTurret) / (maxDistance + neonbroom.cameraDistanceBehindTurret)) * bunny.size
          local shieldSize = (1 - (bunny.shieldedBy.distance + neonbroom.cameraDistanceBehindTurret) / (maxDistance + neonbroom.cameraDistanceBehindTurret)) * bunny.shieldedBy.size
          if (distanceSquaredBetweenPhysicalPoints(bunny.shieldedBy, bunny) < (shieldSize/2 + bunnySize/2)) then
            targetable = false
          end
        end
      end
      if (targetable) then
        local thetaDiff = bunny.theta - player.currentFacing
        while (thetaDiff < -math.pi) do thetaDiff = thetaDiff + 2*math.pi end
        while (thetaDiff > math.pi) do thetaDiff = thetaDiff - 2*math.pi end
        
        if (math.abs(thetaDiff) <= player.fov) then 
          local xbunny, ybunny, scalebunny, _ = neonbroom.getObjectSimulatedScreenCoordsOptAngle(bunny, player.currentFacing, false, 0)
          
          local reticleDistanceToBunny = (xbunny - xReticle)^2 + (ybunny - yReticle)^2 - (scalebunny/2 + ((widthReticle + heightReticle) / 2) / 2)^2
          if reticleDistanceToBunny > maxReticleDistanceToTarget then
             bunny.lockOnStartTime = nil
          else
            bunny.lockOnStartTime = bunny.lockOnStartTime or gameTime
            if (bunny.lockOnStartTime + player.bulletTemplate.lockOnTime < gameTime and numTargetsCycled < maxTargetsToCycle) then
              numTargetsCycled = numTargetsCycled + 1
              if not player.target or (player.target.lastShotAtTime or 0) > (bunny.lastShotAtTime or 0) then
                player.target = bunny
              end
            end
          end
        end
      end
    end
  end
  
  --shoots bullets
  local canShoot = player.shotCooldownUntil < neonbroom.getTimeInSeconds()
  local tryingToShoot = playdate.buttonIsPressed(playdate.kButtonA)
  
  if canShoot and tryingToShoot then
    local target = player.target
    if (target) then
      player.target.lastShotAtTime = neonbroom.getTimeInSeconds()
    else
      target = {}
        local reticleThetaOffset = ((player.aimingReticle.xPosPlaydate / (neonbroom.ratios.playdate.playAreaWidthScale * neonbroom.ratios.playdate.width)) - .5) * player.fov
        target.theta = player.currentFacing + reticleThetaOffset
        target.height = player.aimingReticle.yPosPlaydate / neonbroom.ratios.playdate.height * viewport.height / neonbroom.ratios.playdate.playAreaHeightScale
        target.distance = 100
    end
    local bullet = makeBullet(player.bulletTemplate, target)
    player.shotCooldownUntil = neonbroom.getTimeInSeconds() + player.bulletTemplate.shotCooldown
  end
     
  --moves bullets
  for i, bullet in ipairs(level.bullets) do
    if (bullet.target) then
      neonbroom.updateVelocityFromTo(bullet, bullet.target)
    end
    
    if bullet.distance >= maxDistance or bullet.height > viewport.height or bullet.height < 0 or (bullet.travelRange and bullet.travelRange > bullet.maxRange) then
      bullet.remove = true
    else
      local normalPhysics = true
      local oldTheta = bullet.theta
      local oldHeight = bullet.height
      local oldDistance = bullet.distance
      if (bullet.target) then        
        local distanceTraveledSquared = (bullet.hVelocity*dt)^2 + (bullet.dVelocity*dt)^2
        local distanceToTargetSquared = distanceSquaredBetweenPhysicalPoints(bullet, bullet.target)
        if (distanceToTargetSquared < distanceTraveledSquared) then
          normalPhysics = false
          bullet.height = bullet.target.height
          bullet.distance = bullet.target.distance
          bullet.theta = bullet.target.theta
        end
      end
      if (normalPhysics) then
        if (bullet.fliesStraight) then
          bullet.xPos = bullet.xPos+ bullet.xVelocity * dt
          bullet.yPos = bullet.yPos+ bullet.yVelocity * dt
          bullet.zPos = bullet.zPos+ bullet.zVelocity * dt
          bullet.theta, bullet.height, bullet.distance = cartesianToPolar(bullet.xPos, bullet.yPos, bullet.zPos)
        else          
          bullet.height = bullet.height + bullet.hVelocity * dt
          bullet.distance = bullet.distance + bullet.dVelocity * dt
          bullet.theta = bullet.theta + bullet.tVelocity * dt
        end
      end
      
      bullet.angle = bullet.angle + bullet.rotation * dt
      
      if (bullet.maxRange > 0) then
        bullet.travelRange = (bullet.travelRange or 0) + math.sqrt((bullet.height-oldHeight)^2 + (bullet.distance-oldDistance)^2 + ((bullet.theta-oldTheta)*bullet.distance)^2)
      end
      
      if (bullet.distance < 0) then
        bullet.theta = bullet.theta + math.pi
        bullet.distance = -1 * bullet.distance
        bullet.dVelocity = -1 * bullet.dVelocity
      end
    end
  end
  
  --makes bullet collisions happen
  checkBulletBunnyCollisions(level.bullets, level.bunnies)
  if (boss) then 
    checkBulletBunnyCollisions(level.bullets, boss.bunnies) 
    checkBulletBunnyCollisions(level.bullets, {boss}) 
  end

  neonbroom.removeFlagged(level.bullets, true)
  neonbroom.removeFlagged(level.bunnies, true)
  if (boss) then neonbroom.removeFlagged(boss.bunnies, true) end
  neonbroom.removeFlagged(level.plants, false)
end

function moveBunniesAndCheckForPlants(bunnies, dt)
  local gameTime = neonbroom.getTimeInSeconds()
  
  for i, bunny in ipairs(bunnies) do
    local hit = false
    local thetaDiff = 0
    
    
    -- checks to see if any bunny collides with a plant
    if (bunny.targetPlant and not bunny.targetPlant.remove) then
      thetaDiff = bunny.theta - bunny.targetPlant.theta
      while (thetaDiff < -math.pi) do thetaDiff = thetaDiff + 2*math.pi end
      while (thetaDiff > math.pi) do thetaDiff = thetaDiff - 2*math.pi end
      thetaDiff = math.abs(thetaDiff)*bunny.distance
      
      local heightDiff = math.abs(bunny.height - bunny.targetPlant.height)
      local distanceDiff = math.abs(bunny.distance - bunny.targetPlant.distance)
      
      if thetaDiff <= 2 and heightDiff <= 2 and distanceDiff <= 2 then
        hit = true
      end
    end
    
    if not hit then
      local takingAlternativeAction = false
      
      if (bunny.mirroredTeleportMinCooldown and bunny.state ~= "preeating" and bunny.state ~= "eating" and bunny.state ~= "posteating") then
        if (bunny.state == "disappearing") then
          takingAlternativeAction = true
          if (bunny.stateStartTime + bunny.mirroredTeleportCastTime < gameTime) then
            bunny.height = math.random(5,60)
            bunny.theta = bunny.targetPlant.theta + thetaDiff2(bunny.theta, bunny.targetPlant.theta)
            neonbroom.setState(bunny, "reappearing")
          end
        elseif (bunny.state == "reappearing") then
          takingAlternativeAction = true
          if (bunny.stateStartTime + bunny.mirroredTeleportCastTime < gameTime) then
            bunny.lastTeleportCompletionTime = gameTime
            neonbroom.setState(bunny, "left")
          end
        elseif (bunny.lastTeleportCompletionTime + bunny.mirroredTeleportMinCooldown < gameTime) then
          local chance = (gameTime - (bunny.lastTeleportCompletionTime + bunny.mirroredTeleportMinCooldown)) / (bunny.mirroredTeleportMaxCooldown - bunny.mirroredTeleportMinCooldown)
          if (math.random() < chance) then
            takingAlternativeAction = true
            neonbroom.setState(bunny, "disappearing")
            bunny.tVelocity = 0
            bunny.hVelocity = 0
            bunny.dVelocity = 0
          end
        end
      end
      
      if (not takingAlternativeAction) then
        bunny.theta = bunny.theta + bunny.tVelocity * dt
        bunny.height = bunny.height + bunny.hVelocity * dt
        bunny.distance = bunny.distance + bunny.dVelocity * dt
        bunny.angle = bunny.angle + bunny.rotation * dt
      
        local refObj = bunny
        local refX, refY, refScale, _ = neonbroom.getObjectScreenCoords(refObj)
        for _, child in ipairs(bunny.childAnimations or {}) do
          local xPos, yPos, scale, _ = neonbroom.getObjectScreenCoords(child)
          local touchingSeparation = (refScale + scale) / 2
          local goalSeparation = touchingSeparation * child.lagRatio
          local actualSeparation = math.sqrt((refX-xPos)^2+(refY-yPos)^2)
          if (actualSeparation > goalSeparation) then
              
            local getCloserRatio = goalSeparation / actualSeparation
            
            local tDiff = child.theta - refObj.theta
            while (tDiff < -math.pi) do tDiff = tDiff + 2*math.pi end
            while (tDiff > math.pi) do tDiff = tDiff - 2*math.pi end
            local hDiff = child.height - refObj.height
            local distDiff = child.distance - refObj.distance

            child.theta = refObj.theta + tDiff * getCloserRatio
            child.height = refObj.height + hDiff * getCloserRatio
            child.distance = refObj.distance + distDiff * getCloserRatio

            if (bunny.targetPlant and bunny.animations.left) then
              if (thetaDiff <= 0) then neonbroom.setState(child, "left") else neonbroom.setState(child, "right") end
            end
          end
          
          refObj, refX, refY, refScale = child, xPos, yPos, scale
        end
      end
    else
      bunny.remove = true
      bunny.targetPlant.remove = true
      neonbroom.setState(bunny, "happy")
      bunny.removeAnimationAfter = neonbroom.getTimeInSeconds() + 2.5
      table.insert(level.nonInteractiveAnimations, bunny)
      neonbroom.shakeStepsRemaining = 10
      
      level.bunnyMoneyEarned = level.bunnyMoneyEarned + bunny.bunnyMoneyValue
    end
  end
end

function checkBulletBunnyCollisions(bullets, bunnies)
  for k, bullet in ipairs(bullets) do
    local bulletSize = (1 - (bullet.distance + neonbroom.cameraDistanceBehindTurret) / (maxDistance + neonbroom.cameraDistanceBehindTurret)) * bullet.size
    for i, bunny in ipairs(bunnies) do
      if not bullet.remove and bunny ~= bullet.source then
        local bunnySize = (1 - (bunny.distance + neonbroom.cameraDistanceBehindTurret) / (maxDistance + neonbroom.cameraDistanceBehindTurret)) * bunny.size

        if (distanceSquaredBetweenPhysicalPoints(bullet, bunny) < (bulletSize/2 + bunnySize/2)) then
          if (bunny.shieldedBy and bunny.shieldedBy.hp > 0) then
            local shieldSize = (1 - (bunny.shieldedBy.distance + neonbroom.cameraDistanceBehindTurret) / (maxDistance + neonbroom.cameraDistanceBehindTurret)) * bunny.shieldedBy.size
            if (distanceSquaredBetweenPhysicalPoints(bunny.shieldedBy, bunny) < (shieldSize/2 + bunnySize/2)) then
              bunny = bunny.shieldedBy
            end
          end
          
          bunny.hp = bunny.hp - bullet.damage
          
          if ((bunny.preEatTime or 0 > 0) and (bunny.state ~= "eating" and bunny.state ~= "preeating")) then
            neonbroom.setState(bunny, "preeating")
          else 
            neonbroom.setState(bunny, "eating")
          end
          bunny.chewTime = bullet.chewTime
          
          if bunny.hp <= 0 then
            bunny.remove = true
            bunny.removeAnimationAfter = neonbroom.getTimeInSeconds() + 2.5 + bullet.chewTime
            table.insert(level.nonInteractiveAnimations, bunny)
            
            level.bunnyMoneyEarned = (level.bunnyMoneyEarned or 0) + bunny.bunnyMoneyValue
          end
          
          for i = 1, (bullet.shrapnelCount or 0) do
            local target = {}
              target.theta = bunny.theta + (math.random() - 0.5)/100*math.pi
              target.height = bunny.height + math.random() - 0.5
              target.distance = bunny.distance + math.random() - 0.5
            makeBullet(bullet.shrapnelBulletTemplate, target, bunny)
          end
          
          bullet.remove = true
        end  
      end      
    end
  end
end

drawPlaying = function()  
    --compute the UI scale  
  --draws a bunch of stars
  gfx.setColor(gfx.kColorBlack) -- the background is black and we draw white on it.
  gfx.fillRect(0, 0, 400, 240)
  
  gfx.setColor(gfx.kColorWhite) -- the background is black and we draw white on it.

  for i, star in ipairs(stars) do   -- loop through all of our stars
    PlaydateTheta = (star.theta - player.currentFacing + math.pi) % (2 * math.pi) - math.pi
    scaledTheta = (PlaydateTheta + (player.fov / 2)) / player.fov
    xPos = scaledTheta * windowWidth
    
    gfx.drawPixel(xPos, star.height)   -- draw each point
  end
  
  --draws the horizon very crudely
  playdate.graphics.fillRect(0, 160, 400, 80)
  
  -- prepares all physicalObjects in order of distance.
  objectsToDraw = {}
  for i, obj in ipairs(level.bunnies) do
    table.insert(objectsToDraw, obj)
    for i2, o2 in ipairs(obj.childAnimations or {}) do
      table.insert(objectsToDraw, o2)
    end
  end
  for i, obj in ipairs(level.bullets) do
    table.insert(objectsToDraw, obj)
  end
  for i, obj in ipairs(level.plants) do
    table.insert(objectsToDraw, obj)
  end
  for i, obj in ipairs(level.nonInteractiveAnimations) do
    if (obj.state == "preeating" and obj.lastSetTime + obj.preEatTime < neonbroom.getTimeInSeconds()) then
      neonbroom.setState(obj, "eating")
    end
    if (obj.state == "eating" and obj.lastSetTime + obj.chewTime < neonbroom.getTimeInSeconds()) then
      neonbroom.setState(obj, "happy")
    end
      
    if (obj.removeAnimationAfter) then
      if (neonbroom.getTimeInSeconds() > obj.removeAnimationAfter) then 
        obj.remove = true 
      end
    elseif neonbroom.getAnimationRepeatCount(obj) >= 1 then
      obj.remove = true
    end
    
    if (not obj.remove) then
      table.insert(objectsToDraw, obj)
      for i2, o2 in ipairs(obj.childAnimations or {}) do
        table.insert(objectsToDraw, o2)
      end
    end
  end
  if (boss) then
    table.insert(objectsToDraw, boss)
    for i2, o2 in ipairs(boss.childAnimations or {}) do
      table.insert(objectsToDraw, o2)
    end
    
    for i, obj in ipairs(boss.bunnies) do
      table.insert(objectsToDraw, obj)
      for i2, o2 in ipairs(obj.childAnimations or {}) do
        table.insert(objectsToDraw, o2)
      end
    end
  end
  
  neonbroom.removeFlagged(level.nonInteractiveAnimations, false)
    
  table.sort(objectsToDraw, function(a,b) return a.distance > b.distance end)
  
  -- draws the drawables
  for i, obj in ipairs(objectsToDraw) do
    neonbroom.drawPhysicalObject(obj)
  end
  
  if player.target then
    player.lockOnReticle.theta = player.target.theta
    player.lockOnReticle.height = player.target.height
    player.lockOnReticle.distance = player.target.distance
    player.lockOnReticle.size = player.target.size
    player.lockOnReticle.scale = 1.5
    
    neonbroom.drawPhysicalObject(player.lockOnReticle)
  end
  
  --updates the gui
  neonbroom.radarFovLeft.angle = player.currentFacing - player.fov / 2
  neonbroom.radarFovRight.angle = player.currentFacing + player.fov / 2

  --draws the gui
  neonbroom.drawGuiObject(player.aimingReticle)
  neonbroom.drawGuiObject(neonbroom.hudBase)
  neonbroom.drawGuiObject(neonbroom.radarBackground)
  neonbroom.drawGuiObject(neonbroom.radarFovLeft)
  neonbroom.drawGuiObject(neonbroom.radarFovRight)
  
  neonbroom.healthBar.imageArrayPos = math.max(1, 1 + math.floor(#level.plants / level.numberOfPlants * 60)) -- calculates how many pixels the health bar should show based on the oldPlyr's health (max is to keep it from going below zero)
  neonbroom.drawGuiObject(neonbroom.healthBar)
  
  --draws the radar blips
  for i, obj in ipairs(objectsToDraw) do
    if (obj.guiRadarBlip) then
      local objBlipDistanceRatio = obj.distance / maxDistance
      local objBlipTheta = obj.theta - math.pi / 2
      
      local xPosRadarRelative = math.cos(objBlipTheta) * objBlipDistanceRatio * neonbroom.radarBackground.widthPlaydate / 2
      obj.guiRadarBlip.xPosPlaydate = xPosRadarRelative + neonbroom.radarBackground.xPosPlaydate
      
      local yPosRadarRelative = math.sin(objBlipTheta) * objBlipDistanceRatio * neonbroom.radarBackground.heightPlaydate / 2
      obj.guiRadarBlip.yPosPlaydate = yPosRadarRelative + neonbroom.radarBackground.yPosPlaydate    
      
      neonbroom.drawGuiObject(obj.guiRadarBlip)
    end
  end
  
  --draws the turret dome
  oldPlyr.turretDome.imageArrayPos = #oldPlyr.turretDome.images -  math.floor(player.currentFacing % (2 * math.pi) / (2 * math.pi) * #oldPlyr.turretDome.images + 0.5) % #oldPlyr.turretDome.images
  neonbroom.drawGuiObject(oldPlyr.turretDome)

  --Lets us know the game is over
  if level.ended then
    if currentLevelDefinition.didYouWin() then
      local newSeedWon = level.results.newSeedType
      local newSeedCount = level.results.newSeedCount
      local newSeedsText = ""
      if (newSeedWon) then
        newSeedsText = "\nAlso, now you can research "..newSeedCount.." "..newSeedWon.." seeds!"
      end

      local msg = "You Win This Time, Bun-Tex!\nYou had " ..#level.plants.. " carrots left! A new world record probably!\nYou earned " ..level.bunnyMoneyEarned.. " Bunny Money, for a total of " ..profile().wallet.bunnyMoney.."!\nPress any key to go back to the main menu!"..newSeedsText
      gfx.drawText(msg, 160, 72)
    else
      
      gfx.drawText("GAME OVER\nYou lasted " ..math.floor(timePlayed - startTime) .." seconds! A new world record probably!\nYou earned " ..level.bunnyMoneyEarned.. " Bunny Money!\nYou have " ..profile().wallet.bunnyMoney.." total Bunny Money!\nPress any key to go back to the main menu!", 200, 120)
    end
    
    saveProfiles()    
  end
  
  if (boss) then
    gfx.drawText("BOSS HP "..boss.hp, 80, 48)
  end

end
