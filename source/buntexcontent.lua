import "neonbroom"
import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"

local gfx <const> = playdate.graphics


upgrades = {"aimingreticlespeed", "widerfov", "peagunbulletspeed", "peagunshotcooldown", "carrotgunlockonspeed", "carrotgunbulletspeed"}
upgradeIndexLookup = neonbroom.invertITable(upgrades)
upgradeDetails = {
  aimingreticlespeed={maxLevel=10, baseCost = 50, maxBenefit = neonbroom.ratios.playdate.width * 0.50, currencyType="bunnyMoney", name="Reticle Speed", description="Faster reticle means less time not feeding bunnies!"},
  widerfov={maxLevel=10, maxBenefit = math.pi / 4, baseCost = 20, currencyType="bunnyMoney", name="Wider FOV", description="Larger field of view means less cranking around!"},
  peagunbulletspeed={maxLevel=10, maxBenefit = 200, unlock="peaGun", baseCost = 15, currencyType="peaPennies", name="Pea Flight Speed", description="Peas go so fast."}, 
  peagunshotcooldown={maxLevel=10, maxBenefit = 0.1, unlock="peaGun", baseCost = 15, currencyType="peaPennies", name="Peagun Cooldown", description="So many peas!"}, 
  carrotgunlockonspeed={maxLevel=10, maxBenefit = 0.5, unlock="carrotGun", baseCost = 15, currencyType="carrotCoins", name="Carrotgun Lock On Speed", description="Less waiting for lock on, yay."}, 
  carrotgunbulletspeed={maxLevel=10, maxBenefit = 0.3, unlock="carrotGun", baseCost = 15, currencyType="carrotCoins", name="Carrot Flight Speed", description="Faster carrots wooooo!"},
  cabbagegunlockonspeed={maxLevel=10, maxBenefit = 0.5, unlock="cabbageGun", baseCost = 15, currencyType="cabbageCash", name="Cabbagegun Lock On Speed", description="Less waiting for lock on, yay."}, 
  cabbagegunbulletspeed={maxLevel=10, maxBenefit = 0.3, unlock="cabbageGun", baseCost = 15, currencyType="cabbageCash", name="Cabbage Flight Speed", description="Faster cabbages wooooo!"},
}
currencyTypes = {"bunnyMoney", "peaPennies", "carrotCoins", "cabbageCash"}
currencyDetails = {
  bunnyMoney={displayName = "Bunny Money"},
  peaPennies={displayName = "Pea Pennies"},
  carrotCoins={displayName = "Carrot Coins"},
  cabbageCash={displayName = "Cabbage Cash"},
}
plantTypes = {"pea", "carrot", "cabbage"}
plantDetails = {
  pea = {inverseBunnyAttractionValues = {testBunny3 = 0, floppyBunny = 0.99, centipedeBunny = 1, magicBunny = 1, shieldBunny = 1, camoBunny=1, shyBunny=1}, bunnyCount = 7, protectValue = 1, protectCurrencyType = "peaPennies", name="Pea Plant Seeds", description = "The pea is often underappreciated as a vegetable. While a single pea may not satisfy the average bunny, one must simply supply a steady stream of them to fill that tummy."},
  carrot = {inverseBunnyAttractionValues = {testBunny3 = 0, floppyBunny = 0.9, centipedeBunny = 0.99, magicBunny=0.99, shieldBunny = 1, camoBunny=1, shyBunny=1}, unlocks="carrotGun", bunnyCount = 10, protectValue = 1, protectCurrencyType = "carrotCoins", name="Carrot Plant Seeds", description = "The quintessential bunny food."},
  cabbage = {inverseBunnyAttractionValues = {testBunny3 = 0, floppyBunny = 0.40, centipedeBunny = 0.90, magicBunny=0.99, shieldBunny = 0.99, camoBunny=1, shyBunny=1}, unlocks="cabbageGun", bunnyCount = 12, protectValue = 1, protectCurrencyType = "cabbageCash", name="Cabbage Plant Seeds", description = "So green. So crisp."},
}
gunTypes = {"peaGun", "carrotGun", "cabbageGun", "avocadoGun"}
gunIndexLookup = neonbroom.invertITable(gunTypes)
gunDetails = {
  peaGun = {baseSpeed=520,baseDamage=.5,baseShotCooldown=0.2,baseSize=5,baseLockOnTime=.05,baseChewTime=3},
  carrotGun = {baseSpeed=95,baseDamage=2,baseShotCooldown=0.6,baseSize=15,baseLockOnTime=1.1,baseChewTime=6},
  cabbageGun = {baseSpeed=75,baseDamage=3,baseShotCooldown=0.6,baseSize=15,baseLockOnTime=.2,baseChewTime=9,baseShrapnelCount=15,shrapnelBullet="cabbageShrapnel"},
  cabbageShrapnel = {baseSpeed=150,baseDamage=1,baseShotCooldown=0,baseSize=5,baseLockOnTime=0,baseChewTime=3,baseMaxRange=15,fliesStraight=true},
  avocadoGun = {baseSpeed=60,baseDamage=4,baseShotCooldown=1,baseSize=12,baseLockOnTime=1,baseChewTime=15},
}
bunnyTypes = {"testBunny3", "floppyBunny", "centipedeBunny", "magicBunny", "shyBunny", "shieldBunny", "camoBunny"}
bunnyDetails = {
  testBunny3 = {size=30,speed=2,hp=2,goalHeightOffset=40,bunnyMoneyValue=1},
  floppyBunny = {size=40,speed=5,hp=5,goalHeightOffset=20,bunnyMoneyValue=2},
  centipedeBunny = {size=20,speed=6,hp=2,goalHeightOffset=15,bunnyMoneyValue=3,childAnimations={{name="body",count=25,frames=4,size=20,lagRatio=0.5}}},
  magicBunny = {size=25,speed=3,hp=7,goalHeightOffset=50,bunnyMoneyValue=3,mirroredTeleportMinCooldown=4,mirroredTeleportMaxCooldown=8,mirroredTeleportCastTime=1},
  shyBunny = {size=40,speed=5,hp=5,goalHeightOffset=20,bunnyMoneyValue=2,avoidsFov=true},
  shieldBunny = {size=40,speed=8,hp=5,goalHeightOffset=0,goalDistanceOffset=-2,bunnyMoneyValue=10,protectBunnies=true},
  camoBunny = {size=25,speed=2,hp=10,goalHeightOffset=0,initialHeight=100,bunnyMoneyValue=12,hasDirectionlessFlight=true,preEatTime=0.5,postEatTime=0.5},
}

bosses = {
  pea = {
    bunnyMoneyValue = 50,
    load = function()
      neonbroom.loadAnimation(bosses.pea, "flying", "bunnies/harrierbunny/harrierbunnyflying", 4, 10)
      neonbroom.loadAnimation(bosses.pea, "eating", "bunnies/harrierbunny/harrierbunnyeating", 6, 10)
      neonbroom.loadAnimation(bosses.pea, "happy", "bunnies/harrierbunny/harrierbunnyhappy", 5, 10)
      
      bosses.pea.child = {}
      neonbroom.loadAnimation(bosses.pea.child, "flying", "bunnies/harrierbunny/harrierbunnybodyflying", 8, 10)
      neonbroom.loadAnimation(bosses.pea.child, "opening", "bunnies/harrierbunny/harrierbunnybodybombopening", 5, 5)
      neonbroom.loadAnimation(bosses.pea.child, "releasing", "bunnies/harrierbunny/harrierbunnybodybombopen", 1, 10)
      neonbroom.loadAnimation(bosses.pea.child, "closing", "bunnies/harrierbunny/harrierbunnybodybombclosing", 5, 5)
      bosses.pea.paratrooperTemplate = loadParatrooperBunnyTemplate()
      
    end,
    reset = function()
      boss = {}
      
      boss.bunnies = {}

      boss.funcs = bosses.pea
      
      boss.size = 75
      boss.hp = 75
      boss.theta = 0
      boss.distance = 70
      boss.height = 70
      boss.angle = 0
      
      boss.stage = 1
      
      boss.animations = bosses.pea.animations
      neonbroom.setState(boss, "flying")
      
      boss.lastSpawnCheck = 0
      boss.speed = 250
      
      boss.childAnimations = {}
      boss.childAnimations[1] = {}
      boss.childAnimations[1].animations = bosses.pea.child.animations
      neonbroom.setState(boss.childAnimations[1], "flying")
      boss.childAnimations[1].size = 120
      boss.childAnimations[1].lagRatio = 0
      boss.childAnimations[1].theta = 0
      boss.childAnimations[1].distance = 70.1
      boss.childAnimations[1].height = 70
      boss.childAnimations[1].angle = 0
      
      boss.cycleLength = 6
      boss.cycleTheta = 0
    end,
    doBossUpdate = function(dt)
      local gameTime = neonbroom.getTimeInSeconds()
      if (not boss.lastSpawnTime) then
        boss.lastSpawnTime = gameTime 
        boss.cycleStartTime = gameTime
        boss.lastMoveTime = gameTime
      end
      
      if (boss.hp <= 0) then
        neonbroom.setState(boss, "happy")
        neonbroom.setState(boss.childAnimations[1], "flying")
        return
      end
      
      if (boss.moving) then
        boss.chewTime = 0
        neonbroom.updateVelocityFromTo(boss, boss.target)
        if (boss.lastTotalDistanceTraveled < dt*boss.speed) then
          boss.theta = boss.target.theta
          boss.height = boss.target.height
          boss.distance = boss.target.distance
          boss.moving = false
          boss.lastMoveTime = gameTime
          boss.lastSpawnTime = gameTime 
          boss.cycleStartTime = gameTime
          boss.lastSpawnCheck = gameTime
          boss.cycleTheta = boss.theta
        else
          boss.theta = boss.theta + boss.tVelocity * dt
          boss.height = boss.height + boss.hVelocity * dt
          boss.distance = boss.distance + boss.dVelocity * dt
  --        boss.angle = boss.angle + boss.rotation * dt
        end
        
        if (boss.tVelocity == 0) then
          boss.moving = false
          boss.lastSpawnTime = gameTime 
          boss.cycleStartTime = gameTime
          boss.lastSpawnCheck = gameTime
        end
      else
        if (boss.stage == 1 and boss.hp <= 50) then
          boss.stage = 2
          local cyclePoint = (boss.cycleStartTime - gameTime) % boss.cycleLength / boss.cycleLength
          boss.cycleLength = 4
          boss.cycleStartTime = gameTime - (1 - cyclePoint) * boss.cycleLength
        elseif (boss.stage == 2 and boss.hp <= 25) then
          boss.stage = 3
          local cyclePoint = (boss.cycleStartTime - gameTime) % boss.cycleLength / boss.cycleLength
          boss.cycleLength = 2
          boss.cycleStartTime = gameTime - (1 - cyclePoint) * boss.cycleLength
        end
        
        if (boss.state == "eating" and (neonbroom.getTimeInSeconds() - boss.lastSetTime) >= boss.chewTime) then
          neonbroom.setState(boss, "flying")
        end
        
        if (boss.spawningBunnies) then
          local spawnTime = gameTime - boss.spawnStartTime
          if (spawnTime < 1) then
            neonbroom.setState(boss.childAnimations[1], "opening")
            -- wait for opening
          elseif (spawnTime >= 1 and spawnTime < 4) then
            neonbroom.setState(boss.childAnimations[1], "releasing")
            -- if it's time to make a new one, do so!
            if (boss.nextSpawnTime < gameTime) then
              boss.nextSpawnTime = gameTime + boss.bunnySpawnRate
              local bunny = createBunny(bosses.pea.paratrooperTemplate)
              neonbroom.setState(bunny, "flying")
              bunny.theta = boss.theta
              bunny.height = boss.height
              bunny.distance = boss.distance + 0.11
              bunny.angle = 0
              bunny.parachuteState = 1
              bunny.spawnTime = gameTime
              bunny.detachTime = gameTime + boss.bunnySpawnRate
              table.insert(boss.bunnies, bunny)
            end
          elseif (spawnTime >= 4 and spawnTime < 5) then
            neonbroom.setState(boss.childAnimations[1], "closing")
            -- wait for closing
          else
            neonbroom.setState(boss.childAnimations[1], "flying")
            boss.lastSpawnTime = gameTime
            boss.lastSpawnCheck = gameTime
            boss.spawningBunnies = false
          end
        elseif (gameTime - boss.lastMoveTime > 5 and math.random() < (gameTime - boss.lastMoveTime - 5) / 15) then
          boss.moving = true
          local minThetaChange = math.pi/2
          local goalThetaChange = (2*math.pi - 2*minThetaChange) * math.random() + minThetaChange
          boss.target = {theta=boss.theta + goalThetaChange, distance=boss.distance, height=70}
        else
          if (boss.lastSpawnTime + 1 < gameTime) then
            if (boss.lastSpawnCheck + 0.1 < gameTime) then
              local spawnChance = math.min((gameTime - boss.lastSpawnTime)/100, 1)
              if (math.random() <= spawnChance) then
                boss.spawningBunnies = true
                local mobsToSpawn = boss.stage + 1
                boss.bunnySpawnRate = mobsToSpawn / 3
                boss.nextSpawnTime = gameTime
                boss.spawnStartTime = gameTime
              end
            end
          end
        end
        
        -- boss position!
        local cyclePoint = (boss.cycleStartTime - gameTime) % boss.cycleLength / boss.cycleLength
        rlProp = math.sin(cyclePoint * 2 * math.pi)
        udProp = math.sin(cyclePoint * 4 * math.pi)
        
        boss.theta = boss.cycleTheta + rlProp * math.pi/6
        boss.height = 35 + udProp * 30
      end
      
      boss.childAnimations[1].theta = boss.theta
      boss.childAnimations[1].height = boss.height

      -- parachute bunny movement!
      -- parachuteState = 1 means still connected to the boss
      -- parachuteState = 2 means normal movement
      for _,bunny in ipairs(boss.bunnies) do
        if (bunny.parachuteState == 1) then
          local propToDetach = 1 - ((gameTime - bunny.detachTime) / (bunny.spawnTime - bunny.detachTime))
          local startingHeightOffset = 20
          local goalHeightOffset = 40
          bunny.height = boss.height + startingHeightOffset + propToDetach * (goalHeightOffset - startingHeightOffset)
          bunny.theta = boss.theta
          if (bunny.height > 94) then bunny.remove = true end
          if gameTime >= bunny.detachTime then bunny.parachuteState = 2 end
        elseif (bunny.parachuteState == 2) then
          if (not bunny.targetPlant) then
            bunny.targetPlant = level.plants[math.random(#level.plants)]
            neonbroom.updateVelocityFromTo(bunny, bunny.targetPlant)
          end
          if (bunny.targetPlant.remove and not bunny.remove and bunny.height > 99) then bunny.remove = true end
          -- animations???
        end
      end
      
      neonbroom.removeFlagged(boss.bunnies, true)
    end,
  },  
  carrot = {
    bunnyMoneyValue = 50,
    load = function()
      neonbroom.loadAnimation(bosses.carrot, "blank", "bunnies/UFOBunny/ufobunnyfaceblank", 1, 16)
      
      neonbroom.loadAnimation(bosses.carrot, "flying", "bunnies/UFOBunny/ufobunnyfaceflying", 8, 8)
      neonbroom.loadAnimation(bosses.carrot, "refusingfood", "bunnies/UFOBunny/ufobunnyfacerefusingfood", 4, 8)
      neonbroom.loadAnimation(bosses.carrot, "beaming", "bunnies/UFOBunny/ufobunnyfacebeaming", 8, 10)
      neonbroom.loadAnimation(bosses.carrot, "eating", "bunnies/UFOBunny/ufobunnyfaceeating", 8, 10)
      neonbroom.loadAnimation(bosses.carrot, "happy", "bunnies/UFOBunny/ufobunnyfacehappy", 8, 16)
      
      bosses.carrot.body = {}
      neonbroom.loadAnimation(bosses.carrot.body, "preintro", "bunnies/UFOBunny/ufobunnybodypreintro", 4, 8)
      neonbroom.loadAnimation(bosses.carrot.body, "intro", "bunnies/UFOBunny/ufobunnybodyintro", 8, 4, true)
      neonbroom.loadAnimation(bosses.carrot.body, "flying", "bunnies/UFOBunny/ufobunnybodyflying", 8, 8)
      neonbroom.loadAnimation(bosses.carrot.body, "refusingfood", "bunnies/UFOBunny/ufobunnybodyrefusingfood", 4, 8)
      neonbroom.loadAnimation(bosses.carrot.body, "opening", "bunnies/UFOBunny/ufobunnybodyopening", 5, 5, true)
      neonbroom.loadAnimation(bosses.carrot.body, "closing", "bunnies/UFOBunny/ufobunnybodyclosing", 5, 5, true)
      neonbroom.loadAnimation(bosses.carrot.body, "happy", "bunnies/UFOBunny/ufobunnybodyhappy", 8, 8)

      bosses.carrot.beam = {}
      neonbroom.loadAnimation(bosses.carrot.beam, "goingdown", "bunnies/UFOBunny/ufobunnybeamgoingup", 8, 8, true)
      neonbroom.loadAnimation(bosses.carrot.beam, "beaming", "bunnies/UFOBunny/ufobunnybeammoving", 8, 8)
      neonbroom.loadAnimation(bosses.carrot.beam, "goingup", "bunnies/UFOBunny/ufobunnybeamgoingdown", 8, 8, true)
      neonbroom.loadAnimation(bosses.carrot.beam, "blank", "bunnies/UFOBunny/ufobunnyfaceblank", 1, 10)

      bosses.carrot.plant = {}
      neonbroom.loadAnimation(bosses.carrot.plant, "blurry", "bunnies/UFOBunny/ufobunnyplantblurry", 8, 10)
      neonbroom.loadAnimation(bosses.carrot.plant, "blank", "bunnies/UFOBunny/ufobunnyfaceblank", 1, 10)
    end,
    reset = function()
      boss = {}
      
      boss.bunnies = {}

      boss.funcs = bosses.carrot
      
      boss.size = 25
      boss.hp = 750
      boss.theta = 0
      boss.distance = 100
      boss.height = 10
      boss.angle = 0
      
      boss.stage = 1
      
      boss.animations = bosses.carrot.animations
      neonbroom.setState(boss, "blank")
      
      boss.speed = 10
      boss.name = "carrotBoss"
      
      boss.childAnimations = {}
      boss.childAnimations[1] = {}
      boss.childAnimations[1].animations = bosses.carrot.body.animations
      neonbroom.setState(boss.childAnimations[1], "preintro")
      boss.childAnimations[1].size = 100
      boss.childAnimations[1].lagRatio = 0
      boss.childAnimations[1].theta = 0
      boss.childAnimations[1].distance = boss.distance + 0.1
      boss.childAnimations[1].height = 10
      boss.childAnimations[1].angle = 0
      boss.childAnimations[1].name = "carrotBossBody"
      
      boss.childAnimations[2] = {}
      boss.childAnimations[2].animations = bosses.carrot.plant.animations
      neonbroom.setState(boss.childAnimations[2], "blank")
      boss.childAnimations[2].size = 25
      boss.childAnimations[2].lagRatio = 100000
      boss.childAnimations[2].theta = 0
      boss.childAnimations[2].distance = boss.distance + 0.1
      boss.childAnimations[2].height = 100
      boss.childAnimations[2].angle = 0
      boss.childAnimations[2].name = "carrotBossPlant"

      boss.childAnimations[3] = {}
      boss.childAnimations[3].animations = bosses.carrot.beam.animations
      neonbroom.setState(boss.childAnimations[3], "blank")
      boss.childAnimations[3].size = 100
      boss.childAnimations[3].lagRatio = 100000
      boss.childAnimations[3].theta = 0
      boss.childAnimations[3].distance = boss.distance + 0.2
      boss.childAnimations[3].height = 50
      boss.childAnimations[3].angle = 0
      boss.childAnimations[3].name = "carrotBossBeam"

      boss.cycleLength = 6
      boss.cycleTheta = 0
    end,
    customBulletHitFunction = function(bullet)
      if (boss.state == "beaming") then
        neonbroom.setState(boss, "eating")
        return true
      elseif (boss.state == "flying") then
        neonbroom.setState(boss.childAnimations[1], "flying")
      end
      neonbroom.setState(boss, "refusingfood")      
      return false  
    end,
    doBossUpdate = function(dt)
      local gameTime = neonbroom.getTimeInSeconds()
      
      if (boss.hp <= 0) then
        boss.stage = 6
      elseif (boss.stage == 1) then
        neonbroom.updateVelocityFromTo(boss, {theta=0, distance=50, height=10})
        boss.theta = boss.theta + dt*boss.tVelocity
        boss.distance = boss.distance + dt*boss.dVelocity
        boss.height = boss.height + dt*boss.hVelocity
        boss.childAnimations[1].theta = boss.theta
        boss.childAnimations[1].distance = boss.distance + 0.1
        boss.childAnimations[1].height = boss.height
        if (boss.distance <= 52) then 
          neonbroom.setState(boss.childAnimations[1], "intro")
          boss.stage = 2 
        end
      elseif (boss.stage == 2) then
        if (boss.childAnimations[1].stateStartTime + 2 <= gameTime) then
          boss.stage = 3
          neonbroom.setState(boss.childAnimations[1], "flying")
          neonbroom.setState(boss, "flying")
        end
      elseif (boss.stage == 6) then
        if (boss.childAnimations[3].state == "beaming") then
          boss.childAnimations[2].height = boss.childAnimations[2].height + dt*boss.speed*3
          if (boss.childAnimations[2].height >= 99) then
            table.insert(level.plants, boss.plant)
            boss.plant = nil
            neonbroom.setState(boss.childAnimations[3], "goingup")
          end
        elseif (boss.childAnimations[3].state == "goingup") then
          if (boss.childAnimations[3].stateStartTime + 1 <= gameTime) then
            neonbroom.setState(boss.childAnimations[3], "blank")
            neonbroom.setState(boss.childAnimations[1], "closing")
          end
        elseif (boss.childAnimations[1].state == "closing") then
          if (boss.childAnimations[1].stateStartTime + 1 <= gameTime) then
            neonbroom.setState(boss.childAnimations[1], "flying")
          end
        else
          if (boss.state == "eating" and (neonbroom.getTimeInSeconds() - boss.lastSetTime) >= boss.chewTime) then
            boss.stage = 7
            neonbroom.setState(boss.childAnimations[1], "happy")
            neonbroom.setState(boss, "happy")
          end
        end
      elseif (boss.stage == 7) then
        -- just like, chill there happy until the level is over?
      else -- stages 3,4,5 (actual combat)
        if (boss.state == "eating") then
          if (boss.childAnimations[3].state == "beaming") then
            boss.childAnimations[2].height = boss.childAnimations[2].height + dt*boss.speed*3
            if (boss.childAnimations[2].height >= 99) then
              table.insert(level.plants, boss.plant)
              boss.lastplant = plant
              boss.plant = nil
              neonbroom.setState(boss.childAnimations[3], "goingup")
            end
          elseif (boss.childAnimations[3].state == "goingdown") then
            if (boss.childAnimations[3].stateStartTime + 1 <= gameTime) then
              neonbroom.setState(boss.childAnimations[3], "goingup")
            end            
          elseif (boss.childAnimations[3].state == "goingup") then
            if (boss.childAnimations[3].stateStartTime + 1 <= gameTime) then
              neonbroom.setState(boss.childAnimations[3], "blank")
              neonbroom.setState(boss.childAnimations[1], "closing")
            end            
          elseif (boss.childAnimations[1].state == "closing") then
            if (boss.childAnimations[1].stateStartTime + 1 <= gameTime) then
              neonbroom.setState(boss.childAnimations[1], "flying")
            end
          else
            if (boss.state == "eating" and (gameTime - boss.lastSetTime) >= boss.chewTime) then
              neonbroom.setState(boss, "flying")
            end
          end
        elseif (boss.childAnimations[3].state == "goingdown") then
          if (boss.childAnimations[3].stateStartTime + 1 <= gameTime) then
            if (boss.target.remove) then
              neonbroom.setState(boss, "refusingfood")
              neonbroom.setState(boss.childAnimations[1], "goingup")
            else
              neonbroom.setState(boss, "beaming")
              neonbroom.setState(boss.childAnimations[3], "beaming")
              boss.plant = boss.target
              boss.target.remove = true
              neonbroom.setState(boss.childAnimations[2], "blurry")
              boss.childAnimations[2].theta = boss.plant.theta
              boss.childAnimations[2].distance = boss.plant.distance+0.15
              boss.childAnimations[2].height = 100
            end
          end
        elseif (boss.childAnimations[3].state == "goingup") then
          if (boss.childAnimations[3].stateStartTime + 1 <= gameTime) then
              neonbroom.setState(boss.childAnimations[1], "closing")
              neonbroom.setState(boss.childAnimations[3], "blank")
          end
        elseif (boss.state == "beaming") then
          boss.plant = boss.target
          boss.childAnimations[2].height = boss.childAnimations[2].height - dt*boss.speed
          if (boss.childAnimations[2].height <= 20) then
            boss.plant = nil
            neonbroom.setState(boss, "eating")
            boss.childAnimations[2].height = 100
            neonbroom.setState(boss.childAnimations[2], "blank")
            boss.chewTime = 5
          end
        elseif (boss.childAnimations[1].state == "opening") then
          if (boss.childAnimations[1].stateStartTime + 1 <= gameTime) then
            if (boss.target.remove) then
              neonbroom.setState(boss, "refusingfood")
              neonbroom.setState(boss.childAnimations[1], "closing")
            else
              neonbroom.setState(boss.childAnimations[3], "goingdown")
              boss.childAnimations[3].theta = boss.theta
              boss.childAnimations[3].distance = boss.distance + 0.2
              boss.childAnimations[3].height = 50

            end
          end
        elseif (boss.state == "refusingfood") then
          if (boss.childAnimations[1].stateStartTime + 0.5 <= gameTime) then
            if (boss.childAnimations[1].state == "refusingfood") then 
              neonbroom.setState(boss, "flying")
              neonbroom.setState(boss.childAnimations[1], "flying") 
            end
          end
        else -- moving toward a target...
          if (not boss.target or boss.target.remove or boss.target == boss.lastplant) then
            boss.target = level.plants[math.random(#level.plants)]
          end
          
          if (not boss.target.remove and boss.target ~= boss.lastplant) then 
            neonbroom.updateVelocityFromTo(boss, {distance = boss.target.distance, theta = boss.target.theta, height = 10})
            
            if (boss.lastTotalDistanceTraveled < dt*boss.speed) then
              boss.theta = boss.target.theta
              boss.distance = boss.target.distance
            else
              boss.theta = boss.theta + dt*boss.tVelocity
              boss.distance = boss.distance + dt*boss.dVelocity
              boss.height = boss.height + dt*boss.hVelocity
            end
            boss.childAnimations[1].theta = boss.theta
            boss.childAnimations[1].distance = boss.distance + 0.1
            boss.childAnimations[1].height = boss.height
              
            if (boss.distance == boss.target.distance and boss.theta == boss.target.theta) then
              neonbroom.setState(boss.childAnimations[1], "opening")
            end
          end
        end
      end
    end,
  },
}

levelDefinitions = {}
  levelDefinitions.itemsLoaded = 0
  local levelOneDefinition = {}
  levelOneDefinition.loadLevel = function()
    local itemsPerLoad = 2;

    local itemsAlreadyLoaded = levelDefinitions.itemsLoaded
    levelDefinitions.itemsLoaded = levelDefinitions.itemsLoaded+itemsPerLoad

    level = level or {}
        
    level.bunnyTemplates = level.bunnyTemplates or {}
    for _, bunnyType in ipairs(bunnyTypes) do
      if (itemsAlreadyLoaded > 0) then itemsAlreadyLoaded = itemsAlreadyLoaded - 1
      elseif (itemsAlreadyLoaded > -itemsPerLoad) then 
        itemsAlreadyLoaded = itemsAlreadyLoaded - 1
        level.bunnyTemplates[bunnyType] = _loadBunnyTemplate(bunnyType)
      end
    end
    
    level.plantTemplates = level.plantTemplates or {}
    for _,plantType in ipairs(plantTypes) do
      if (itemsAlreadyLoaded > 0) then itemsAlreadyLoaded = itemsAlreadyLoaded - 1
      elseif (itemsAlreadyLoaded > -itemsPerLoad) then
        itemsAlreadyLoaded = itemsAlreadyLoaded - 1
        if (bosses[plantType]) then bosses[plantType].load() end
        level.plantTemplates[plantType] = {}
        neonbroom.loadAnimation(level.plantTemplates[plantType], "new", "plants/"..plantType:lower().."plant", 1, 10)
      end
    end
    
    if (itemsAlreadyLoaded > -itemsPerLoad) then
      level.plantBlipImage = gfx.image.new("images/radarblipcross")
      return true
    end
    return false
  end
  
  levelOneDefinition.resetLevel = function()
    boss = nil
    
    level.ended = false
    level.playerPaid = false
    
    neonbroom.shakeStepsRemaining = 0
    
    level.nextBunnySpawnTime = 0
    level.bunnyMoneyEarned = 0
    
    level.bullets = {}
    level.bunnies = {}
    level.plants = {}
    level.nonInteractiveAnimations = {}
    
    level.results = {}
    
    computeLevelNewSeedReward()
    
    level.chancesOfNotSpawning = {}
    for _, bunnyType in ipairs(bunnyTypes) do
      level.chancesOfNotSpawning[bunnyType] = computeChanceOfNotSpawning(bunnyType)
    end
    
    local numPeaSeeds = profile().research.peas
    local numCarrotSeeds = profile().research.carrots
    
    level.bunniesCreated = 0
    level.numberOfPlants = 0
    level.numBunnies = 0
    for _, plant in ipairs(plantTypes) do
      local bunnyCount = getPlantBunnyCount(plant)
      local numPlants = (profile().research[plant] or 0)
      
      if (numPlants == 10) then bosses[plant].reset() end
      
      level.numBunnies = level.numBunnies + bunnyCount * numPlants
      level.numberOfPlants = level.numberOfPlants + numPlants
      
      for i = 1, numPlants do
        table.insert(level.plants, makePlant(level.plantTemplates[plant].animations, level.plantBlipImage, plant))
      end
    end
  end
  
  createBunny = function(bunnyTemplate)
    local bunny = {}
    
    for k, v in pairs(bunnyTemplate) do
      if (k == "childAnimations") then
        bunny[k] = {}
        for k2, v2 in ipairs(v) do
          bunny[k][k2] = {}
          for k3, v3 in pairs(v2) do
            bunny[k][k2][k3] = v3
          end
        end
      else
        bunny[k] = v
      end
    end
    
    bunny.speed = bunny.speed * 1 -- bunnies should maybe speed up based on the amount of time the level has been going on
    
    bunny.height = bunnyTemplate.initialHeight or math.random() * 40
    bunny.theta = math.random() * 2 * math.pi
    neonbroom.setState(bunny, bunnyTemplate.state)
    
    bunny.targetable = true
    
    if (bunny.childAnimations) then
      for _,child in ipairs(bunny.childAnimations) do
        child.height = bunny.height
        child.theta = bunny.theta
        child.distance = bunny.distance
        child.angle = 0
      end
    end
    
    return bunny
  end

  levelOneDefinition.levelUpdate = function(dt)
    if (level.ended) then return end
    
    if (boss) then boss.funcs.doBossUpdate(dt) end
    
    -- makes bunnies
    local canSpawnBunny = neonbroom.getTimeInSeconds() > level.nextBunnySpawnTime and level.numBunnies - level.bunniesCreated > 0
    local tryingToSpawnBunny = true
    
    if canSpawnBunny and tryingToSpawnBunny then
      
      local bunnyTemplate = {}
      
      for i = #bunnyTypes, 1, -1 do
        local bunnyType = bunnyTypes[i]
        if math.random() > (level.chancesOfNotSpawning[bunnyType] or 0) then
          bunnyTemplate = level.bunnyTemplates[bunnyType]
          break
        end
      end
      
     bunnyTemplate = level.bunnyTemplates["floppyBunny"] -- testBunny3 floppyBunny
      local bunny = createBunny(bunnyTemplate)
      table.insert(level.bunnies, bunny)
      
   --   bunnyTemplate = level.bunnyTemplates["shieldBunny"]
  --    local bunny2 = createBunny(bunnyTemplate)
 --     bunny2.theta = bunny.theta
--      table.insert(level.bunnies, bunny2)
      
      level.bunniesCreated = level.bunniesCreated + 1
      level.nextBunnySpawnTime = neonbroom.getTimeInSeconds() + computeSpawnDelay() -- MAGIC MATH
      
    end
    
    for i, bunny in ipairs(level.bunnies) do
      if (bunny.shieldedBy and (bunny.shieldedBy.remove or bunny.shieldedBy.hp == 0)) then bunny.shieldedBy = nil end
      
      -- check if bunny is targeting a valid plant
      if (bunny.protectBunnies) then
        if (not bunny.shieldTarget or bunny.shieldTarget.remove) then
          protectableBunnies = {}
          for _, b in ipairs(level.bunnies) do
            if (not b.protectBunnies and not b.shieldedBy) then table.insert(protectableBunnies, b) end
          end
          if (#protectableBunnies > 0) then
            bunny.shieldTarget = protectableBunnies[math.random(#protectableBunnies)]
            bunny.shieldTarget.shieldedBy = bunny
            
          else
            bunny.shieldTarget = nil
          end
        end
      end
    
      if (not bunny.shieldTarget and (not bunny.targetPlant or bunny.targetPlant.remove)) then
        bunny.targetPlant = level.plants[math.random(#level.plants)]
      end
      neonbroom.updateVelocityFromTo(bunny, (bunny.shieldTarget or bunny.targetPlant))
    end
    
    handleLevelEnd()
  end

  levelOneDefinition.didYouWin = function()
    return level.numBunnies - level.bunniesCreated <= 0 and #level.bunnies == 0
  end
  levelDefinitions.level1 = levelOneDefinition

function _loadBunnyTemplate(bunnyType)
  local details = bunnyDetails[bunnyType]
  local template = {}
    template.name = bunnyType
    
    for k, v in pairs(details) do
      template[k] = v
    end
    
    template.guiRadarBlip = {}  -- creates a radar blip for the bunny
      template.guiRadarBlip.image = gfx.image.new("images/radarblipsquare")
      template.guiRadarBlip.xPosPlaydate = neonbroom.radarBackground.xPosPlaydate
      template.guiRadarBlip.yPosPlaydate = neonbroom.radarBackground.yPosPlaydate
      template.guiRadarBlip.widthPlaydate, template.guiRadarBlip.heightPlaydate = template.guiRadarBlip.image:getSize()
      template.guiRadarBlip.angle = 0
    neonbroom.loadAnimation(template, "hardleft", "bunnies/"..bunnyType.."/"..bunnyType:lower().."hardleft", 4, 10)
    neonbroom.loadAnimation(template, "left", "bunnies/"..bunnyType.."/"..bunnyType:lower().."left", 4, 10)
    neonbroom.loadAnimation(template, "right", "bunnies/"..bunnyType.."/"..bunnyType:lower().."right", 4, 10)
    neonbroom.loadAnimation(template, "hardright", "bunnies/"..bunnyType.."/"..bunnyType:lower().."hardright", 4, 10)
    
    neonbroom.loadAnimation(template, "fleeleft", "bunnies/"..bunnyType.."/"..bunnyType:lower().."fleeleft", 8, 8)
    neonbroom.loadAnimation(template, "fleeright", "bunnies/"..bunnyType.."/"..bunnyType:lower().."fleeright", 8, 8)
    neonbroom.loadAnimation(template, "leftsurprised", "bunnies/"..bunnyType.."/"..bunnyType:lower().."leftsurprised", 8, 8, true)
    neonbroom.loadAnimation(template, "rightsurprised", "bunnies/"..bunnyType.."/"..bunnyType:lower().."rightsurprised", 8, 8, true)
    
    neonbroom.loadAnimation(template, "forward", "bunnies/"..bunnyType.."/"..bunnyType:lower().."forward", 15, 8)
    if (not template.animations.forward) then
      neonbroom.loadAnimation(template, "forward", "bunnies/"..bunnyType.."/"..bunnyType:lower().."left", 15, 8)
    end

    neonbroom.loadAnimation(template, "preeating", "bunnies/"..bunnyType.."/"..bunnyType:lower().."preeating", 8, 6)
    neonbroom.loadAnimation(template, "eating", "bunnies/"..bunnyType.."/"..bunnyType:lower().."eating", 8, 30)
    neonbroom.loadAnimation(template, "posteating", "bunnies/"..bunnyType.."/"..bunnyType:lower().."posteating", 8, 6)
    neonbroom.loadAnimation(template, "happy", "bunnies/"..bunnyType.."/"..bunnyType:lower().."happy", 8, 4, true)
    
    neonbroom.loadAnimation(template, "disappearing", "bunnies/"..bunnyType.."/"..bunnyType:lower().."disappearing", 8, 8, true)
    neonbroom.loadAnimation(template, "reappearing", "bunnies/"..bunnyType.."/"..bunnyType:lower().."reappearing", 8, 8, true)

    if (details.childAnimations) then
      template.childAnimations = {}
      for _, details in ipairs(details.childAnimations) do
        for i = 1, (details.count or 1) do
          local child = {}
          child.size = details.size
          child.lagRatio = details.lagRatio
          neonbroom.loadAnimation(child, "left", "bunnies/"..bunnyType.."/"..bunnyType:lower()..details.name:lower().."left", details.frames, 10)
          neonbroom.loadAnimation(child, "right", "bunnies/"..bunnyType.."/"..bunnyType:lower()..details.name:lower().."right", details.frames, 10)
          
          table.insert(template.childAnimations, child)
        end
      end
    end
    
    template.distance = 100
    template.tVelocity = 0
    template.hVelocity = 0
    template.dVelocity = 0
    template.angle = 0
    template.rotation = 0--(math.random() - 0.5) * 2 * math.pi
    
    template.lastTeleportCompletionTime = neonbroom.getTimeInSeconds()
    template.avoidsFovSafe = true
    template.lastShotAtTime = 0
  return(template)
end

function loadParatrooperBunnyTemplate()
  local template = {}
    template.name = bunnyType
    
    template.size = 50
    template.speed = 15
    template.hp = 1
    template.goalHeightOffset = 0
    template.bunnyMoneyValue = 0

    template.guiRadarBlip = {}  -- creates a radar blip for the bunny
      template.guiRadarBlip.image = gfx.image.new("images/radarblipsquare")
      template.guiRadarBlip.xPosPlaydate = neonbroom.radarBackground.xPosPlaydate
      template.guiRadarBlip.yPosPlaydate = neonbroom.radarBackground.yPosPlaydate
      template.guiRadarBlip.widthPlaydate, template.guiRadarBlip.heightPlaydate = template.guiRadarBlip.image:getSize()
      template.guiRadarBlip.angle = 0
    local bunnyType = "paratrooperbunny"
    neonbroom.loadAnimation(template, "flying", "bunnies/"..bunnyType.."/"..bunnyType:lower().."flying", 4, 10)
    neonbroom.loadAnimation(template, "eating", "bunnies/"..bunnyType.."/"..bunnyType:lower().."eating", 8, 30)
    neonbroom.loadAnimation(template, "happy", "bunnies/"..bunnyType.."/"..bunnyType:lower().."happy", 8, 10)
    
    template.tVelocity = 0
    template.hVelocity = 0
    template.dVelocity = 0
    template.angle = 0
    template.rotation = 0
    
    template.lastShotAtTime = 0
    
    return template
  end
  
  function makePlant(animations, radarBlipImg, plantType)
  local plant = {}
    plant.name = plantType
    plant.height = 100
    plant.theta = math.random() * math.pi * 2
    plant.distance = math.random(20, 80)
    plant.tVelocity = 0
    plant.hVelocity = 0
    plant.dVelocity = 0
    plant.animations = animations
    neonbroom.setState(plant, "new")
    plant.size = plant.animations["new"][1]:getSize() / 2
    plant.angle = 0
    
    plant.guiRadarBlip = {}
      plant.guiRadarBlip.image = radarBlipImg
      plant.guiRadarBlip.xPosPlaydate = neonbroom.radarBackground.xPosPlaydate
      plant.guiRadarBlip.yPosPlaydate = neonbroom.radarBackground.yPosPlaydate
      plant.guiRadarBlip.widthPlaydate, plant.guiRadarBlip.heightPlaydate = radarBlipImg:getSize()
      plant.guiRadarBlip.angle = 0
      
    plant.plantType = plantType
    
  return(plant)
end

function computeChanceOfNotSpawning(bunnyType)
  chanceOfNotSpawning = 1
  
  for _, plantType in pairs(plantTypes) do
    for i = 1, (profile().research[plantType] or 0) do
      chanceOfNotSpawning = chanceOfNotSpawning * getInverseBunnyAttractionValue(plantType, bunnyType)
    end
  end
  
  return(chanceOfNotSpawning)
end

function computeSpawnDelay()
  local totalBunnies = level.numBunnies
  local bunniesSpawnedSoFar = level.bunniesCreated
  local totalPlants = level.numberOfPlants
  
  local percentageOfBunniesSpawnedSofar = bunniesSpawnedSoFar / totalBunnies
  
  local additionalPlantImpact = (totalPlants - 3) / 12 -- THIS IS SOOPER IMPORTANT TO OUR MAGIC MATH
  
  if percentageOfBunniesSpawnedSofar < .25 then -- phase 1
    return (4 - (2 * additionalPlantImpact))
  elseif percentageOfBunniesSpawnedSofar < .75 then -- phase 2
    return (3 - (1.5 * additionalPlantImpact))
  else -- phase 3
    return (2 - (1 * additionalPlantImpact))
  end
end

function computeCostToBuyUpgrade(upgrade)
  return math.floor(upgradeDetails[upgrade].baseCost * 2 ^ (profile().upgradeLevels[upgrade] or 0))
end

function canAffordUpgrade(upgrade)
  local cost = computeCostToBuyUpgrade(upgrade)
  local availableFunds = profile().wallet[upgradeDetails[upgrade].currencyType] or 0

  return cost <= availableFunds
end

function computeLevelNewSeedReward()
  local newSeedWon = nil
  local newSeedCount = 0
  for i, plantType in ipairs(plantTypes) do
    if (profile().seeds[plantType] or 0) == 0 then 
      if (newSeedCount == 10) then 
        newSeedWon = plantType
        newSeedCount = 0
      end
      break
    elseif (profile().seeds[plantType] == profile().research[plantType]) then
      newSeedWon = (profile().seeds[plantType] or 0) < 10 and plantType or nil
      newSeedCount = (profile().seeds[plantType] or 0)
    end
  end
  level.results.newSeedType = newSeedWon
  level.results.newSeedCount = newSeedCount + 1
end

function isUpgradeable(upgrade)
  local currentUpgradeLevel = profile().upgradeLevels[upgrade] or 0
  local maxUpgradeLevel = upgradeDetails[upgrade].maxLevel
  
  return currentUpgradeLevel < maxUpgradeLevel
end

function purchaseUpgrade(upgrade)
  if isUpgradeable(upgrade) and canAffordUpgrade(upgrade) then
    profile().wallet[upgradeDetails[upgrade].currencyType] = profile().wallet[upgradeDetails[upgrade].currencyType] - computeCostToBuyUpgrade(upgrade)
    profile().upgradeLevels[upgrade] = (profile().upgradeLevels[upgrade] or 0) + 1
    
    saveProfiles()
  end
end

function getInverseBunnyAttractionValue(plant, bunny)
  return plantDetails[plant].inverseBunnyAttractionValues[bunny] or 1
end

function getPlantBunnyCount(plant)
  return plantDetails[plant].bunnyCount
end

function handleLevelEnd()
  if level.ended then return end
  
  local failed = false
  if #level.plants == 0 then
    neonbroom.shakeStepsRemaining = 32
    level.ended = true
    failed = true
  end
  
  if (level.numBunnies - level.bunniesCreated <= 0 and #level.bunnies == 0) and (not boss or (boss.hp <= 0 and #boss.bunnies == 0)) then
    level.ended = true
  end
  
  if level.ended then
    if (not failed) then
      local newSeedWon = level.results.newSeedType
      local newSeedCount = level.results.newSeedCount
      if (newSeedWon) then
        profile().seeds[newSeedWon] = newSeedCount
        profile().research[newSeedWon] = newSeedCount
        
        local total = 0
        for _, v in pairs(profile().research) do
          total = total + v
        end
        if (total > 15) then
          for _, plantType in ipairs(plantTypes) do
            while (total > 15 and (profile().research[plantType] or 0) > 0) do
              profile().research[plantType] = profile().research[plantType] - 1
              total = total - 1
            end
          end
        end
        
        if (newSeedCount == 6 and plantDetails[newSeedWon].unlocks) then
          profile().isUnlocked[plantDetails[newSeedWon].unlocks] = true
        end
      end
      
      plantCounts = {}
      for _, plant in ipairs(level.plants) do
        plantCounts[plant.plantType] = (plantCounts[plant.plantType] or 0) + 1
      end
      
      for plantType, count in pairs(plantCounts) do
        local earnedCurrencyType = plantDetails[plantType].protectCurrencyType
        local earnedCurrencyValueBase = plantDetails[plantType].protectValue
        local earnedCurrencyValue = earnedCurrencyValueBase * count
        
        level.results[plantType] = {}
        level.results[plantType].protected = count
        level.results[plantType].currencyType = earnedCurrencyType
        level.results[plantType].currencyValue = earnedCurrencyValue
      
        getPaid(earnedCurrencyType, earnedCurrencyValue)
      end
    end
    
    getPaid("bunnyMoney", level.bunnyMoneyEarned)
  end
end

