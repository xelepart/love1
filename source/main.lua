import "neonbroom" -- adds neonbroom.lua
import "buntexcontent"
import "buntexplaying"
import "buntexprofile"
import "buntexplayer"
import "buntexmenus"

import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"

local gfx <const> = playdate.graphics

function setupGame()
  math.randomseed(playdate.getSecondsSinceEpoch())
  
  -- this loads in buntexmenus.lua, when i tried here it didn't work? loadMenus()
  loadPlayerAssets()

  --deprecated concepts from pre-playdate sdk days using love and a normal windowed game
  windowWidth = 400
  windowHeight = 240
  neonbroom.uiScale = 1 -- theoretically allows us to scale the UI for other platforms? which won't ever happen?
  
  currentLevelDefinition = levelDefinitions.level1
  
  gameState = {}
    gameState.currentState = "loading"
    
  loadingScreen = {}
    loadingScreen.image = gfx.image.new("images/loadingscreen.png")
    loadingScreen.xPosPlaydate = neonbroom.ratios.playdate.width / 2
    loadingScreen.yPosPlaydate = neonbroom.ratios.playdate.height / 2
    loadingScreen.widthPlaydate = neonbroom.ratios.playdate.width
    loadingScreen.heightPlaydate = neonbroom.ratios.playdate.height
    loadingScreen.angle = 0
  
  --defines the maximum distance away from the camera as 100 units of awesome
  maxDistance = 100
  distance2DThreshold = maxDistance * 1 -- defines the point at which an object is judged as being in either 3D or 2D for collision purposes
  
  --defines the viewport from which we see the game
  viewport = {}
    viewport.height = 100 -- everything is in these units
    
  --makes a bunch of background stars for testing purposes
  local max_stars = 1000   -- how many stars we want
  stars = {}

  for i = 1, max_stars do   -- generate the coords of our stars
    local theta = math.random() * 2 * math.pi   -- generate a "random" number for the x coord of this star
    local y = math.random(windowHeight)  
    local star = {}
      star.theta = theta
      star.height = y
      
    table.insert(stars, star)
  end  
end

setupGame()

function loadOldPlyr()
  if (oldPlyr) then return true end
  --creates the oldPlyer
  oldPlyr = {}

  oldPlyr.turretDome = {}  --constructs the turret dome down at the bottom of the screen
    oldPlyr.turretDome.images = {}
      for i = 1, 360 do
        if i < 11 then
          oldPlyr.turretDome.images[i] = gfx.image.new("images/turretdomeanimation/dithered/000"..(i-1))
        elseif i < 101 then
          oldPlyr.turretDome.images[i] = gfx.image.new("images/turretdomeanimation/dithered/00"..(i-1))
        else
          oldPlyr.turretDome.images[i] = gfx.image.new("images/turretdomeanimation/dithered/0"..(i-1))
        end
      end
    oldPlyr.turretDome.imageArrayPos = 0
    oldPlyr.turretDome.xPosPlaydate = neonbroom.ratios.playdate.width / 2 * neonbroom.ratios.playdate.playAreaWidthScale
    oldPlyr.turretDome.yPosPlaydate = neonbroom.ratios.playdate.height - 30
    oldPlyr.turretDome.widthPlaydate, oldPlyr.turretDome.heightPlaydate = oldPlyr.turretDome.images[1]:getSize()
    oldPlyr.turretDome.angle = 0

  return true
end

local crankAlertShowing = false    
local lastUpdate = 0

function playdate.update()
  local thisUpdateTime = playdate.getCurrentTimeMilliseconds()
  neonbroom.setUpdateTime(thisUpdateTime)
  local dt = (thisUpdateTime - lastUpdate) / 1000.0
  lastUpdate = thisUpdateTime

  windowWidth = 400
  windowHeight = 240
  neonbroom.uiScale = 1.0

  if gameState.currentState == "playing" then
    currentLevelDefinition.levelUpdate(dt)
    updatePlaying(dt)
  elseif gameState.currentState == "loading" then
    --
  elseif gameState.currentState == "mainmenu" then
    --
  end

  playdate.timer.updateTimers()

  draw()

  if (playdate.isCrankDocked()) then
    if crankAlertShowing then playdate.ui.crankIndicator:update()
    else 
      crankAlertShowing = true
      playdate.ui.crankIndicator:start()
    end
  else
    crankAlertShowing = false
  end
    
  local anyButtonPressed = playdate.buttonIsPressed(playdate.kButtonA) or playdate.buttonIsPressed(playdate.kButtonB) or playdate.buttonIsPressed(playdate.kButtonUp) or playdate.buttonIsPressed(playdate.kButtonDown) or playdate.buttonIsPressed(playdate.kButtonLeft) or playdate.buttonIsPressed(playdate.kButtonRight)
  
  -- button presses:
  if gameState.currentState == "loading" and isMenuLoaded() and currentLevelDefinition.loadLevel() and loadOldPlyr() and anyButtonPressed then
    gameState.currentState = "mainmenu"
  elseif gameState.currentState == "mainmenu" then
    handleMenuKeypresses()
  elseif gameState.currentState == "playing" and canGoBackToMainMenuAfterGameOver and anyButtonPressed then
    gameState.currentState = "mainmenu"
    canGoBackToMainMenuAfterGameOver = false
  elseif gameState.currentState == "playing" and playdate.buttonIsPressed(playdate.kButtonB) then
    level.bunniesCreated = level.numBunnies
  end
end

function draw()
  windowWidth = 400
  windowHeight = 240
  neonbroom.uiScale = 1.0

  if gameState.currentState == "playing" then
    drawPlaying()
  elseif gameState.currentState == "loading" then
    neonbroom.drawGuiObject(loadingScreen)
  elseif gameState.currentState == "mainmenu" then
    _drawMenu()
  end
end


