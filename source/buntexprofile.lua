import "neonbroom"
import "CoreLibs/object"
import "CoreLibs/graphics"
import "CoreLibs/sprites"
import "CoreLibs/timer"

local gfx <const> = playdate.graphics


local profiles, possibleError = playdate.datastore.read("dataforbuntex")

profile = function()
  return profiles[profiles.currentProfile]
end

getUpgradeLevel = function(upgrade)
  return profile().upgradeLevels[upgrade] or 0
end

saveProfiles = function()
  playdate.datastore.write(profiles, "dataforbuntex", true)
end

updateStatsInProfile = function(statName, addIn)
  profile().stats[statName] = (profile().stats[statName] or 0) + addIn
end

getPaid = function(currencyType, amount)
  profile().wallet[currencyType] = (profile().wallet[currencyType] or 0) + amount
  updateStatsInProfile(currencyType, amount)
end

function levelOneProfile() 
  profiles = {}
  profiles.currentProfile = 1
  defaultProfile = {}
  table.insert(profiles, defaultProfile)

  --stats (total bunnies fed, total ground plants eaten, ratios?)
  --max level you've gotten to
  --whatever our upgrade situation is
  defaultProfile.name = "Lazersaurus"
  defaultProfile.totalEarnedBunnyMoney = 100
  defaultProfile.bunniesFed = 0
  defaultProfile.currentGun = "peaGun"
  
  defaultProfile.stats = {}
  defaultProfile.seeds = {}
    defaultProfile.seeds.pea = 0
    defaultProfile.seeds.carrot = 0
  
  defaultProfile.research = {}
    defaultProfile.research.pea = 3
    defaultProfile.research.carrot = 0
    
  defaultProfile.isUnlocked = {}
    defaultProfile.isUnlocked.peaGun = true
    defaultProfile.isUnlocked.carrotGun = false
    
  defaultProfile.wallet = {}
    defaultProfile.wallet.bunnyMoney = 500
    defaultProfile.wallet.peaPennies = 4
    defaultProfile.wallet.carrotCoins = 0
    
  defaultProfile.upgradeLevels = {}
    defaultProfile.upgradeLevels.widerfov = 0

  saveProfiles()
end

function midGameProfile() 
  profiles = {}
  profiles.currentProfile = 1
  defaultProfile = {}
  table.insert(profiles, defaultProfile)

  --stats (total bunnies fed, total ground plants eaten, ratios?)
  --max level you've gotten to
  --whatever our upgrade situation is
  defaultProfile.name = "Lazersaurus"
  defaultProfile.totalEarnedBunnyMoney = 100
  defaultProfile.bunniesFed = 0
  defaultProfile.currentGun = "peaGun"
  
  defaultProfile.stats = {}
  defaultProfile.seeds = {}
    defaultProfile.seeds.pea = 10
    defaultProfile.seeds.carrot = 5
  
  defaultProfile.research = {}
    defaultProfile.research.pea = 3
    defaultProfile.research.carrot = 4
    
  defaultProfile.isUnlocked = {}
    defaultProfile.isUnlocked.peaGun = true
    defaultProfile.isUnlocked.carrotGun = true
    
  defaultProfile.wallet = {}
    defaultProfile.wallet.bunnyMoney = 50
    defaultProfile.wallet.peaPennies = 0
    defaultProfile.wallet.carrotCoins = 0
    
  defaultProfile.upgradeLevels = {}
    defaultProfile.upgradeLevels.widerfov = 4

  saveProfiles()
end

if (true or not profiles) then
  levelOneProfile()
  -- midGameProfile()
end
