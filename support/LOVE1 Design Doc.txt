LOVE1 aka BUN-TEX vs. the COSMIC CRAVING: A game about shooting a turret at stuff

Credits: Rachelle Adams - Triangle Wizard

Overview: You are BUN-TEX, the sheriff of a lonely space-western planetoid called the BUNNYMOON. You must protect the moon from a horde of hangry hares by firing carrots into their mouths. There are a lot of geopolitical complexities involved.

Stuff we need to put in the game:

Aiming reticle:

- Circle thingy with lines going into it that follows your WASD commands
- and also locks onto target
- and also has a little lock on thing that starts big and gets smaller as it hits target

Turret:

- Semicircle at the bottom of the screen with a rectangle sticking out of it that points at the reticle

DONE Bullets:

- Circles that go from the rectangle to the reticle when you hit a button

DONE Targets:

- Circles with hitboxes that disappear when they get hit with bullets

DONE Collision:

- Makes bullets crash into monsters

DONE Lives/energy/hit points

- How to make game overs happen
- Screen shake on hit?? DONE

Fucking like maybe some fucking real art? Shit.

- Sprites and shit
- Backgrounds? Starfields? Little bunny-type cities?

Multiple monster types:

- How to add variety to the game
- Regular angry rabbit
- Deluxe-sized angry rabbit
- Rabbit tentacle that has rabbit mouths instead of suckers
- Rabbit land shark - turns into a rabbit goldfish
- Rabbit milipede that walks on a thousand rabbit ears instead of legs

- Monster hit points

Multiple bullet types

- Also how to add variety to the game
- Surface-to-surface bullets (mortars? turnips?)
- Explosive bullets (pumpkins?)
- Rapid-fire bullets (peas?)
- Chain lightning (magic carrots that hit multiple enemies depending on how close they are to each other)

Bonus items

- Extra shields/lives/energy
- Increased shot power/frequency


UPGRADE SYSTEM v1

- Start with one pea plant on the ground
- Every bunny you feed gives you Bunny Money/Rabbit Research, which can be used to upgrade the player
- Every plant you save gets you UNNAMED PLANT CURRENCY, which upgrades the associated weapon and lets you buy more seeds of that type/improves the seed packet you can get from that type
- Seed packets for better weapon types are gated behind bosses
- Plant a certain number of seeds of a given type to unlock a boss level
- Each type of vegetable attracts a different type of enemy
- Planting two or more types of seed gives synergy bonuses to both and attracts additional enemies and enemy types








Bosses

- Big things at the ends of waves
- Big rabbit? Weird angel rabbit? Attack helicopter rabbit? fuckin Harrier jet rabbit? Giant jackelope?

Final boss

- Big cool satisfying endgame
- Final boss: THE COSMIC CRAVING -- total eldrich horror except it's a rabbit

Upgrade system

- How to make replayability
- Get points/money/karma for shooting, spend it on stuff
- Better armor/guns, faster crank, more life/lives, etc.


*****PIE IN THE SKY CORNER*****

OTHER GAME IDEAS WE HAD BECAUSE OF BAD MISTAKES WE MADE:
  - game where you're drawing your shields. (from a bug with bullets)
  
  - BUN-TEX 2: The DIMENSIONAL SCREW
  - YOU ARE ON A SCREW, so FULL ROTATIONS go up and down!
  - each full rotation puts you in a NEW DIMENSION (where you see stuff but can't hit it?)

  - game where you flip the Playdate on its side and turn the crank??

  - clone spaceteam with a BUNCHA FUCKEN CRANKS

  - BUN-TEX 3: DUAL SHOT
  - Two-player co-op Bun-Tex adventure
  - One person cranks, other person shoots?

  - ZELDA WITH A FUCKIN CHAINSAW